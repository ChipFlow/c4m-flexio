from itertools import product

from pdkmaster.technology import geometry as geo
from pdkmaster.design import layout as lay


def guardring(*,
    lib, net, type_, width, height, fill_well=False, fill_implant=False,
):
    assert not ((type_== "p") and fill_well)
    comp = lib.computed
    fab = lib.layoutfab
    metal1 = comp.metal[1].prim
    layout = fab.new_layout()

    left = -0.5*width
    right = 0.5*width
    bottom = -0.5*height
    top = 0.5*height

    ring_width = comp.guardring_width

    if type_ == "n":
        implant = lib.clampnmos.implant[0]
        inner_implant = lib.clamppmos.implant[0]
        nwell = lib.clamppmos.well
        cont_well_args = {"well_net": net, "bottom_well": nwell}
        active_well_args = {"well_net": net, "well": nwell}
    elif type_ == "p":
        implant = lib.clamppmos.implant[0]
        inner_implant = lib.clampnmos.implant[0]
        cont_well_args = {}
        active_well_args = {}
        nwell = None
    else:
        raise AssertionError("Internal error")

    bottom_args = fab.spec4bound(
        via=comp.contact, bound_spec={
            "bottom_left": left,
            "bottom_bottom": bottom,
            "bottom_right": right,
            "bottom_top": bottom + ring_width,
        }
    )
    wire_args = fab.spec4bound(bound_spec={
        "left": left, "bottom": bottom,
        "right": right, "top": bottom + ring_width,
    })
    layout.add_wire(
        net=net, wire=comp.contact, bottom=comp.active, bottom_implant=implant,
        **cont_well_args, **bottom_args,
    )
    l_act = layout.add_wire(
        net=net, wire=comp.active, implant=implant,
        **active_well_args, **wire_args,
    )
    if fill_well:
        assert nwell is not None
        actwell_bounds1 = l_act.bounds(mask=nwell.mask)
    else:
        actwell_bounds1 = None
    layout.add_wire(net=net, wire=metal1, **wire_args)

    bottom_args = fab.spec4bound(
        via=comp.contact, bound_spec={
            "bottom_left": left,
            "bottom_bottom": bottom + ring_width + comp.contact.min_space,
            "bottom_right": left + ring_width,
            "bottom_top": top - ring_width - comp.contact.min_space,
        }
    )
    wire_args = fab.spec4bound(bound_spec={
        "left": left, "bottom": bottom + ring_width,
        "right": left + ring_width, "top": top - ring_width,
    })
    layout.add_wire(
        net=net, wire=comp.contact, bottom=comp.active, bottom_implant=implant,
        **cont_well_args, **bottom_args,
    )
    layout.add_wire(
        net=net, wire=comp.active, implant=implant,
        **active_well_args, **wire_args,
    )
    layout.add_wire(net=net, wire=metal1, **wire_args)

    bottom_args = fab.spec4bound(
        via=comp.contact, bound_spec={
            "bottom_left": right - ring_width,
            "bottom_bottom": bottom + ring_width + comp.contact.min_space,
            "bottom_right": right,
            "bottom_top": top - ring_width - comp.contact.min_space,
        }
    )
    wire_args = fab.spec4bound(bound_spec={
        "left": right - ring_width, "bottom": bottom + ring_width,
        "right": right, "top": top - ring_width,
    })
    layout.add_wire(
        net=net, wire=comp.contact, bottom=comp.active, bottom_implant=implant,
        **cont_well_args, **bottom_args,
    )
    layout.add_wire(
        net=net, wire=comp.active, implant=implant,
        **active_well_args, **wire_args,
    )
    layout.add_wire(net=net, wire=metal1, **wire_args)

    bottom_args = fab.spec4bound(
        via=comp.contact, bound_spec={
            "bottom_left": left,
            "bottom_bottom": top - ring_width,
            "bottom_right": right,
            "bottom_top": top,
        }
    )
    wire_args = fab.spec4bound(bound_spec={
        "left": left, "bottom": top - ring_width, "right": right, "top": top,
    })
    layout.add_wire(
        net=net, wire=comp.contact, bottom=comp.active, bottom_implant=implant,
        **cont_well_args, **bottom_args,
    )
    l_act = layout.add_wire(
        net=net, wire=comp.active, implant=implant,
        **active_well_args, **wire_args,
    )
    layout.add_wire(net=net, wire=metal1, **wire_args)
    if fill_well:
        assert nwell is not None
        assert actwell_bounds1 is not None
        actwell_bounds2 = l_act.bounds(mask=nwell.mask)
        wire_args = fab.spec4bound(bound_spec={
            "left": actwell_bounds1.left,
            "bottom": actwell_bounds1.bottom,
            "right": actwell_bounds1.right,
            "top": actwell_bounds2.top,
        })
        layout.add_wire(net=net, wire=nwell, **wire_args)

    if fill_implant:
        min_impl_space = lib.tech.computed.min_space(comp.active, inner_implant)

        layout.add_primitive(
            prim=inner_implant,
            x=0.0, width=(width - 2*ring_width - 2*min_impl_space),
            y=0.0, height=(height - 2*ring_width - 2*min_impl_space),
        )

    layout.boundary = geo.Rect(left=left, bottom=bottom, right=right, top=top)

    return layout


def clamp(*,
    name="clamp", lib, ckt, type_,
    source_net, drain_net, gate_nets, bulk_net=None
):
    comp = lib.computed
    fab = lib.layoutfab
    layout = fab.new_layout()
    if type_ == "n":
        mos = lib.clampnmos
        w = lib.clampnmos_w
        l = lib.clampnmos_l
        implant = mos.implant[0]
        well = None
        ch_well_args = act_well_args = {}
    elif type_ == "p":
        mos = lib.clamppmos
        w = lib.clamppmos_w
        l = lib.clamppmos_l
        implant = mos.implant[0]
        well = mos.well
        ch_well_args = {"bottom_well": well, "well_net": source_net}
        act_well_args = {"well": well, "well_net": source_net}
    else:
        raise AssertionError("Internal error: clamp mos type")
    if bulk_net is None:
        bulk_net = source_net

    ngates = len(gate_nets)
    gate_pitch = (
        comp.contact.width + lib.clampgate_sourcecont_space
        + l + lib.clampgate_draincont_space
    )
    active_bottom = -0.5*w
    active_top = 0.5*w

    clampdrain_bottom = active_bottom - lib.clampdrain_active_ext
    clampdrain_top = active_top + lib.clampdrain_active_ext

    x_ch0 = -0.5*ngates*gate_pitch
    y_ch_bottom = active_bottom - lib.clampgate_gatecont_space - 0.5*comp.contact.width
    y_ch_top = active_top + lib.clampgate_gatecont_space + 0.5*comp.contact.width
    
    actmos_bounds = None
    active_left = None
    for i, gate_net in enumerate(gate_nets):
        x_ch = x_ch0 + i*gate_pitch
        if (i % 2) == 0:
            ch_net = sd1_net = source_net
            sd2_net = drain_net
            x_trans = (
                x_ch + 0.5*comp.contact.width + lib.clampgate_sourcecont_space
                + 0.5*l
            )
            clampdrain_left = x_trans + 0.5*l - lib.clampgate_clampdrain_overlap
            clampdrain_right = (
                x_trans + 0.5*l + lib.clampgate_draincont_space - lib.clampdrain_gatecont_space
            )
        else:
            ch_net = sd1_net = drain_net
            sd2_net = source_net
            x_trans = (
                x_ch + 0.5*comp.contact.width + lib.clampgate_draincont_space
                + 0.5*l
            )
            clampdrain_left = (
                x_trans - 0.5*l - lib.clampgate_draincont_space + lib.clampdrain_gatecont_space
            )
            clampdrain_right = x_trans - 0.5*l + lib.clampgate_clampdrain_overlap

        # Draw source-drain contact column
        l_actch = layout.add_wire(
            net=ch_net, wire=comp.contact, **ch_well_args,
            bottom=comp.active, bottom_implant=implant,
            x=x_ch, **fab.spec4bound(
                via=comp.contact, bound_spec={
                    "bottom_bottom": active_bottom,
                    "bottom_top": active_top,
                }
            ),
        )
        actch_bounds = l_actch.bounds(mask=comp.active.mask)
        if i == 0:
            active_left = actch_bounds.left
        # If not first connect with active of last drawn transistor
        if actmos_bounds:
            layout.add_wire(
                net=ch_net, wire=comp.active, **act_well_args,
                implant=implant, **fab.spec4bound(bound_spec={
                    "left": actmos_bounds.right,
                    "bottom": actmos_bounds.bottom,
                    "right": actch_bounds.right,
                    "top": actmos_bounds.top,
                }),
            )

        # Draw the gate
        inst = ckt.new_instance(f"{name}_g{i}", mos, l=l, w=w)
        sd1_net.childports += inst.ports.sourcedrain1
        sd2_net.childports += inst.ports.sourcedrain2
        gate_net.childports += inst.ports.gate
        bulk_net.childports += inst.ports.bulk
        l_mos = layout.add_primitive(
            portnets={
                "sourcedrain1": sd1_net, "sourcedrain2": sd2_net,
                "gate": gate_net, "bulk": bulk_net,
            },
            prim=mos, x=x_trans, y=0.0, l=l, w=w,
        )
        actmos_bounds = l_mos.bounds(mask=comp.active.mask)
        polymos_bounds = l_mos.bounds(mask=comp.poly.mask)
        # Connect to active of source-drain contact column
        layout.add_wire(
            net=ch_net, wire=comp.active, **act_well_args,
            implant=implant, **fab.spec4bound(bound_spec={
                "left": actch_bounds.left,
                "bottom": actmos_bounds.bottom,
                "right": actmos_bounds.left,
                "top": actmos_bounds.top,
            }),
        )
        # Draw the extra drain layer
        layout.add_shape(prim=lib.clamp_clampdrain, shape=geo.Rect(
            left=clampdrain_left, bottom=clampdrain_bottom,
            right=clampdrain_right, top=clampdrain_top,
        ))

        # Bottom gate contact
        l_polych = layout.add_wire(
            net=gate_net, wire=comp.contact, bottom=comp.poly,
            x=x_trans, y=y_ch_bottom,
        )
        polych_bounds = l_polych.bounds(mask=comp.poly.mask)
        m1ch_bounds1 = l_polych.bounds(mask=comp.metal[1].prim.mask)
        if polych_bounds.top < polymos_bounds.bottom:
            layout.add_wire(
                net=gate_net, wire=comp.poly, **fab.spec4bound(
                    bound_spec={
                        "left": polymos_bounds.left,
                        "bottom": polych_bounds.top,
                        "right": polymos_bounds.right,
                        "top": polymos_bounds.bottom,
                    },
                )
            )
        # Top gate contact
        l_polych = layout.add_wire(
            net=gate_net, wire=comp.contact, bottom=comp.poly,
            x=x_trans, y=y_ch_top,
        )
        polych_bounds = l_polych.bounds(mask=comp.poly.mask)
        m1ch_bounds2 = l_polych.bounds(mask=comp.metal[1].prim.mask)
        if polych_bounds.bottom > polymos_bounds.top:
            layout.add_wire(
                net=gate_net, wire=comp.poly, **fab.spec4bound(
                    bound_spec={
                        "left": polymos_bounds.left,
                        "bottom": polymos_bounds.top,
                        "right": polymos_bounds.right,
                        "top": polych_bounds.bottom,
                    },
                )
            )
        # Connect both contact with M1
        layout.add_wire(
            net=gate_net, wire=comp.metal[1].prim, **fab.spec4bound(
                bound_spec={
                    "left": m1ch_bounds1.left,
                    "bottom": m1ch_bounds1.top,
                    "right": m1ch_bounds1.right,
                    "top": m1ch_bounds2.bottom,
                },
            )
        )

    # Draw rightmost source-drain contact column
    x_ch = x_ch0 + ngates*gate_pitch
    if (ngates % 2) == 0:
        ch_net = source_net
    else:
        ch_net = drain_net

    l_actch = layout.add_wire(
        net=ch_net, wire=comp.contact, **ch_well_args,
        bottom=comp.active, bottom_implant=implant,
        x=x_ch, **fab.spec4bound(
            via=comp.contact, bound_spec={
                "bottom_bottom": active_bottom,
                "bottom_top": active_top,
            }
        ),
    )
    actch_bounds = l_actch.bounds(mask=comp.active.mask)
    active_right = actch_bounds.right
    if actmos_bounds:
        layout.add_wire(
            net=ch_net, wire=comp.active, **act_well_args,
            implant=implant, **fab.spec4bound(bound_spec={
                "left": actmos_bounds.right,
                "bottom": actmos_bounds.bottom,
                "right": actch_bounds.right,
                "top": actmos_bounds.top,
            }),
        )

    # Draw gate marker layer around the whole clamp
    l_extra = []
    encs = [lib.clampdrain_active_ext]
    if mos.gate.oxide is not None:
        l_extra.append(mos.gate.oxide)
        if mos.gate.min_gateoxide_enclosure is not None:
            encs.append(mos.gate.min_gateoxide_enclosure.max())
    if mos.gate.inside is not None:
        l_extra.extend(mos.gate.inside)
        if mos.gate.min_gateinside_enclosure is not None:
            encs.extend(enc.max() for enc in mos.gate.min_gateinside_enclosure)
    if l_extra:
        enc = max(encs)
        assert active_left is not None
        r = geo.Rect(
            left=(active_left - enc), bottom=(active_bottom - enc),
            right=(active_right + enc), top=(active_top + enc),
        )

        for extra in l_extra:
            layout.add_shape(prim=extra, shape=r)

    return layout


def diamondvia(*,
    lib, net, via, width, height, space, enclosure, center_via, corner_distance,
):
    assert (len(via.bottom) == 1) and (len(via.top) == 1)
    metal_down = via.bottom[0]
    metal_up = via.top[0]

    layout = lib.layoutfab.new_layout()

    enc = enclosure.max()
    mbounds = geo.Rect.from_size(width=width, height=height)
    vbounds = geo.Rect.from_rect(rect=mbounds, bias=-enc)

    def away_from_corner(xy):
        x = xy[0]
        y = xy[1]
        via_left = x - via.width//2
        via_bottom = y - via.width//2
        via_right = via_left + via.width
        via_top = via_bottom + via.width

        d_left = via_left - vbounds.left
        d_bottom = via_bottom - vbounds.bottom
        d_right = vbounds.right - via_right
        d_top = vbounds.top - via_top

        return not any((
            corner_distance - d_left > d_bottom, # In bottom-left corner
            corner_distance - d_right > d_bottom, # In bottom-right corner
            corner_distance - d_left > d_top, # In top-left corner
            corner_distance - d_right > d_top, # In bottom-left corner
        ))

    via_pitch = via.width + space
    vwidth = vbounds.right - vbounds.left
    vheight = vbounds.top - vbounds.bottom
    if center_via:
        # Odd number of vias in horizontal and vertical direction
        nx = int(((vwidth - via.width)/via_pitch)//2)*2 + 1
        ny = int(((vheight - via.width)/via_pitch)//2)*2 + 1
    else:
        # Even number of vias in horizontal and vertical direction
        nx = int(((vwidth + space)/via_pitch)//2)*2
        ny = int(((vheight + space)/via_pitch)//2)*2


    real_width = nx*via_pitch - space
    real_height = ny*via_pitch - space
    real_left = vbounds.left + 0.5*(vwidth - real_width) + 0.5*via.width
    real_bottom = vbounds.bottom + 0.5*(vheight - real_height) + 0.5*via.width

    via_xy = (
        (real_left + i*via_pitch, real_bottom + j*via_pitch)
        for i, j in product(range(nx), range(ny))
    )

    layout.add_shape(prim=via, net=net, shape=geo.MultiShape(shapes=(
        geo.Rect.from_size(
            center=geo.Point(x=x, y=y), width=via.width, height=via.width,
        )
        for x, y in filter(away_from_corner, via_xy)
    )))
    layout.add_shape(prim=metal_down, shape=mbounds),
    layout.add_shape(prim=metal_up, shape=mbounds),

    layout.boundary = mbounds

    return layout
