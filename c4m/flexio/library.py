from math import floor, ceil
from collections import namedtuple
from itertools import product, chain
from typing import Generator, Iterable, Optional, Union, cast

from pdkmaster.typing import IntFloat
from pdkmaster import _util
from pdkmaster.technology import (
    property_ as prp, primitive as prm, geometry as geo,
)
from pdkmaster.design import circuit as ckt_, layout as lay, library as lbry

from . import _helpers as hlp

__all__ = ["IOLibrary"]

_MetalSpec = namedtuple(
    "_MetalSpec", (
        "prim",
        "minwidth_down", "minwidth4ext_down", "minwidth_up", "minwidth4ext_up",
        "minwidth_updown", "minwidth4ext_updown",
    ),
)


def _iterate_polygonbounds(*,
    polygon: Union[lay.MaskPolygon, geo.MaskShape],
) -> Generator[geo._Rectangular, None, None]:
    if isinstance(polygon, lay.MaskPolygon):
        for polygon2 in polygon.polygons:
            yield polygon2.bounds
    elif isinstance(polygon, geo.MaskShape):
        for shape in polygon.shape.pointsshapes:
            yield shape.bounds



class _ComputedSpecs:
    def __init__(self, lib, *, nmos, pmos, ionmos, iopmos):
        assert isinstance(lib, IOLibrary), "Internal error"
        self.lib = lib
        tech = lib.tech

        # assert statements are used for unsupported technology configuration
        # TODO: Implement unsupported technology configurations
              
        assert nmos.well is None
        assert pmos.well is not None
        assert ionmos.well is None
        assert iopmos.well is not None
        assert pmos.well == iopmos.well

        prims = tech.primitives
        self.nmos = nmos
        self.pmos = pmos
        self.ionmos = ionmos
        self.iopmos = iopmos

        assert nmos.gate == pmos.gate
        assert ionmos.gate == iopmos.gate

        mosgate = nmos.gate
        iomosgate = ionmos.gate

        assert mosgate.oxide is None
        assert iomosgate.oxide is not None
        assert mosgate.active == iomosgate.active
        assert mosgate.poly == iomosgate.poly

        self.active = active = mosgate.active
        self.oxide = iomosgate.oxide
        self.poly = poly = mosgate.poly

        assert active.min_substrate_enclosure is not None

        assert len(nmos.implant) == 1
        self.nimplant = nimplant = nmos.implant[0]
        nimplant_idx = active.implant.index(nimplant)
        nimplant_enc = active.min_implant_enclosure[nimplant_idx]
        assert len(pmos.implant) == 1
        self.pimplant = pimplant = pmos.implant[0]
        pimplant_idx = active.implant.index(pimplant)
        pimplant_enc = active.min_implant_enclosure[pimplant_idx]
        assert len(ionmos.implant) == 1
        self.ionimplant = ionimplant = ionmos.implant[0]
        ionimplant_idx = active.implant.index(ionimplant)
        ionimplant_enc = active.min_implant_enclosure[ionimplant_idx]
        assert len(iopmos.implant) == 1
        self.iopimplant = iopimplant = iopmos.implant[0]
        assert pimplant == iopimplant
        iopimplant_idx = active.implant.index(iopimplant)
        iopimplant_enc = active.min_implant_enclosure[iopimplant_idx]

        min_space_active_poly = tech.computed.min_space(poly, active)
        self.min_space_nmos_active = max((
            active.min_space,
            nmos.computed.min_polyactive_extension + min_space_active_poly,
            nmos.min_gateimplant_enclosure[0].max()
              + max(pimplant_enc.max(), lib.tech.computed.min_space(pimplant, active)),
        ))
        self.min_space_pmos_active = max((
            active.min_space,
            pmos.computed.min_polyactive_extension + min_space_active_poly,
            pmos.min_gateimplant_enclosure[0].max()
              + max(nimplant_enc.max(), lib.tech.computed.min_space(nimplant, active)),
        ))

        oxidx = active.oxide.index(iomosgate.oxide)
        self.iogateoxide_enclosure = oxenc = iomosgate.min_gateoxide_enclosure
        # TODO: add active oxide enclosure
        oxext = oxenc.second
        self.activeoxide_enclosure = oxenc = active.min_oxide_enclosure[oxidx]
        if oxenc is not None:
            oxext = max((oxext, oxenc.max()))
        min_space_iomosgate_active = (
            oxext + tech.computed.min_space(iomosgate.oxide, active)
        )
        self.min_space_ionmos_active = max((
            active.min_space,
            ionmos.computed.min_polyactive_extension + min_space_active_poly,
            ionmos.min_gateimplant_enclosure[0].max()
              + iopimplant_enc.max(),
            min_space_iomosgate_active,
        ))
        self.min_space_iopmos_active = max((
            active.min_space,
            iopmos.computed.min_polyactive_extension + min_space_active_poly,
            iopmos.min_gateimplant_enclosure[0].max()
              + ionimplant_enc.max(),
            min_space_iomosgate_active,
        ))

        self.vias = vias = tuple(prims.__iter_type__(prm.Via))
        metals = tuple(prims.__iter_type__(prm.MetalWire))

        # Vias are sorted in the technology from bottom to top
        # so first via is the contact layer
        self.contact = contact = vias[0]
        assert (
            (active in contact.bottom) and (poly in contact.bottom)
            and (len(contact.top) == 1)
        ), "Unsupported configuration"

        actidx = contact.bottom.index(active)
        self.chact_enclosure = actenc = contact.min_bottom_enclosure[actidx]
        polyidx = contact.bottom.index(poly)
        self.chpoly_enclosure = polyenc = contact.min_bottom_enclosure[polyidx]
        self.chm1_enclosure = contact.min_top_enclosure[0]

        self.minwidth_activewithcontact = (
            contact.width + 2*actenc.min()
        )
        self.minwidth4ext_activewithcontact = (
            contact.width + 2*actenc.max()
        )

        self.minwidth_polywithcontact = (
            contact.width + 2*polyenc.min()
        )
        self.minwidth4ext_polywithcontact = (
            contact.width + 2*polyenc.max()
        )

        self.minnmos_contactgatepitch = (
            0.5*contact.width + nmos.computed.min_contactgate_space
            + 0.5*nmos.computed.min_l
        )
        self.minpmos_contactgatepitch = (
            0.5*contact.width + pmos.computed.min_contactgate_space
            + 0.5*pmos.computed.min_l
        )

        self.minionmos_contactgatepitch = (
            0.5*contact.width + ionmos.computed.min_contactgate_space
            + 0.5*ionmos.computed.min_l
        )
        self.miniopmos_contactgatepitch = (
            0.5*contact.width + iopmos.computed.min_contactgate_space
            + 0.5*iopmos.computed.min_l
        )

        self.nwell = nwell = pmos.well
        nwellidx = active.well.index(nwell)
        self.activenwell_minspace = nactenc = active.min_substrate_enclosure.max()
        self.activenwell_minenclosure = pactenc = active.min_well_enclosure[nwellidx].max()

        self.guardring_width = w = contact.width + contact.min_space
        s = max(
            pactenc + nactenc,
            0.5*(nwell.min_space + 2*pactenc - w), # Minimum NWELL spacing
        )
        self.guardring_space = 2*ceil(s/(2*lib.tech.grid))*lib.tech.grid
        self.guardring_pitch = self.guardring_width + self.guardring_space

        self.maxnmos_w = round(
            lib.corerow_pwell_height - (
                0.5*self.minwidth_activewithcontact
                + self.min_space_nmos_active + nactenc
            ),
            6,
        )
        self.maxnmos_y = round(
            lib.iorow_height + 0.5*self.minwidth_activewithcontact
            + self.min_space_nmos_active + 0.5*self.maxnmos_w,
            6,
        )
        self.maxnmos_activebottom = round(
            self.maxnmos_y - 0.5*self.maxnmos_w,
            6,
        )
        self.maxnmos_activetop = round(
            self.maxnmos_y + 0.5*self.maxnmos_w,
            6,
        )
 
        self.maxpmos_w = round(
            lib.corerow_nwell_height - (
                pactenc + self.min_space_pmos_active
                + 0.5*self.minwidth_activewithcontact
            ),
            6,
        )
        self.maxpmos_y = round(
            lib.iorow_height + lib.corerow_pwell_height
            + pactenc + 0.5*self.maxpmos_w,
            6,
        )
        self.maxpmos_activebottom = round(
            self.maxpmos_y - 0.5*self.maxpmos_w,
            6,
        )
        self.maxpmos_activetop = round(
            self.maxpmos_y + 0.5*self.maxpmos_w,
            6,
        )

        self.maxionmos_w = round(
            lib.iorow_pwell_height - (
                nactenc + self.min_space_ionmos_active
                + 0.5*self.minwidth_activewithcontact
            ),
            6,
        )
        self.maxionmos_y = round(
            lib.iorow_nwell_height + nactenc + 0.5*self.maxionmos_w,
            6,
        )
        self.maxionmos_activebottom = round(
            self.maxionmos_y - 0.5*self.maxionmos_w,
            6,
        )
        self.maxionmos_activetop = round(
            self.maxionmos_y + 0.5*self.maxionmos_w,
            6,
        )

        self.maxiopmos_w = round(
            lib.iorow_nwell_height - (
                0.5*self.minwidth_activewithcontact
                + self.min_space_iopmos_active + pactenc
            ),
            6,
        )
        self.maxiopmos_y = round(
            0.5*self.minwidth_activewithcontact + self.min_space_iopmos_active
            + 0.5*self.maxiopmos_w,
            6,
        )
        self.maxiopmos_activebottom = round(
            self.maxiopmos_y - 0.5*self.maxiopmos_w,
            6,
        )
        self.maxiopmos_activetop = round(
            self.maxiopmos_y + 0.5*self.maxiopmos_w,
            6,
        )

        self.io_oxidebottom = (
            0.5*self.minwidth_activewithcontact
            + tech.computed.min_space(iomosgate.oxide, active)
        )
        self.io_oxidetop = (
            lib.iorow_height
            - 0.5*self.minwidth_activewithcontact
            - tech.computed.min_space(iomosgate.oxide, active)
        )

        # Also get dimensions of io transistor in the core row
        self.maxionmoscore_w = round(
            lib.corerow_pwell_height - (
                nactenc + self.min_space_ionmos_active
                + 0.5*self.minwidth_activewithcontact
            ),
            6,
        )
        self.maxionmoscore_y = round(
            lib.iorow_height + 0.5*self.minwidth_activewithcontact
            + self.min_space_ionmos_active + 0.5*self.maxionmoscore_w,
            6,
        )
        self.maxionmoscore_activebottom = round(
            self.maxionmoscore_y - 0.5*self.maxionmoscore_w,
            6,
        )
        self.maxionmoscore_activetop = round(
            self.maxionmoscore_y + 0.5*self.maxionmoscore_w,
            6,
        )

        self.maxiopmoscore_w = round(
            lib.corerow_nwell_height - (
                0.5*self.minwidth_activewithcontact
                + self.min_space_iopmos_active + pactenc
            ),
            6,
        )
        self.maxiopmoscore_y = round(
            lib.iorow_height + lib.corerow_pwell_height
            + pactenc + 0.5*self.maxiopmoscore_w,
            6,
        )
        self.maxiopmoscore_activebottom = round(
            self.maxiopmoscore_y - 0.5*self.maxiopmoscore_w,
            6,
        )
        self.maxiopmoscore_activetop = round(
            self.maxiopmoscore_y + 0.5*self.maxiopmoscore_w,
            6,
        )
        
        for via in vias[1:]:
            assert (
                (len(via.bottom) == 1) and (len(via.top) == 1)
            ), "Unsupported configuration"

        def _metalspec(i):
            downvia = vias[i-1]
            downw = downvia.width
            downenc = downvia.min_top_enclosure[0]
            minwdown = downw + 2*downenc.min()
            minwextdown = downw + 2*downenc.max()
            try:
                upvia = vias[i+1]
            except:
                minwup = None
                minwextup = None
                minwupdown = minwdown
                minwextupdown = minwextdown
            else:
                upw = upvia.width
                upenc = upvia.min_bottom_enclosure[0]
                minwup = upw + 2*upenc.min()
                minwextup = upw + 2*upenc.max()
                minwupdown = max(minwdown, minwup)
                minwextupdown = max(minwextdown, minwextup)

            return _MetalSpec(
                metals[i-1],
                minwdown, minwextdown, minwup, minwextup, minwupdown, minwextupdown,
            )

        def _topmetalspec():
            via = vias[-1]
            w = via.width
            enc = via.min_bottom_enclosure[0]
            minwupdown = minwdown = w + 2*enc.min()
            minwextupdown = minwextdown = w + 2*enc.max()
            minwup = None
            minwextup = None
            return _MetalSpec(
                metals[len(vias) - 1],
                minwdown, minwextdown, minwup, minwextup, minwupdown, minwextupdown,
            )

        self.metal = {i: _metalspec(i) for i in range(1, len(vias))}
        self.metal[len(vias)] = _topmetalspec()

        pads = tuple(tech.primitives.__iter_type__(prm.PadOpening))
        assert len(pads) == 1
        self.pad = pads[0]


class _IOCellFrame:
    """Default cells for in IO cell framework"""
    def __init__(self,
        lib: "IOLibrary",
        pad_y: IntFloat, nclamp_y: IntFloat, pclamp_y: IntFloat,
        secondiovss_y: IntFloat, vddvss_y: IntFloat,
    ):
        assert isinstance(lib, IOLibrary), "Internal error"
        self.lib = lib
        self._pad = None # Only create pad first time it is accessed
        self.pad_y = _util.i2f(pad_y)

        self.nclamp_y = _util.i2f(nclamp_y)
        self.pclamp_y = _util.i2f(pclamp_y)
        self.secondiovss_y = _util.i2f(secondiovss_y)
        self.vddvss_y = _util.i2f(vddvss_y)

        l_gd = lib.get_cell("GateDecode").layout
        gd_height = l_gd.boundary.top - l_gd.boundary.bottom
        self.cells_y = lib.iocell_height - gd_height

    @property
    def pad(self):
        if self._pad is None:
            lib = self.lib
            self._pad = lib.pad(width=lib.pad_width, height=lib.pad_height)
        return self._pad

class _GuardRing(lbry._Cell):
    def __init__(self, *, name, lib, type_, width, height, fill_well, fill_implant):
        super().__init__(lib, name)
        ckt = self.new_circuit()
        conn = ckt.new_net("conn", external=True)
        layouter = self.new_circuitlayouter()

        l = hlp.guardring(
            lib=lib, net=conn, type_=type_, width=width, height=height,
            fill_well=fill_well, fill_implant=fill_implant,
        )
        layouter.place(l, x=0.0, y=0.0)
        self.layout.boundary = l.boundary


class _IODCTrackSegment(lbry._Cell):
    def __init__(self, *, name, lib, height):
        super().__init__(lib, name)

        comp = lib.computed
        track_vias = comp.vias[3:]

        ckt = self.new_circuit()
        track = ckt.new_net("track", external=True)
        layouter = self.new_circuitlayouter()
        fab = layouter.fab

        # Compute the subtracks dimensions
        maxw = lib.ioring_maxwidth
        space = lib.ioring_space
        if height <= maxw:
            n_tracks = 1
            width = height
        else:
            maxp = maxw + space
            n_tracks = int((height - lib.tech.grid - maxw)/(maxp)) + 2
            width = (height - (n_tracks - 1)*space)/n_tracks
            width = int(width/lib.tech.grid)*lib.tech.grid
        pitch = width + space
        self.subtracks_dims = subtracks_dims = (
            *((-0.5*height + i*pitch, -0.5*height + i*pitch + width)
              for i in range(n_tracks - 1)
            ),
            (0.5*height - width, 0.5*height)
        )
        topspace = lib.ioring_spacetop
        spacediff = (topspace - space)
        self.topsubtracks_dims = topsubtracks_dims = tuple(
            (bottom + 0.5*spacediff, top - 0.5*spacediff)
            for bottom, top in subtracks_dims
        )

        for i, track_via in enumerate(track_vias):
            is_top = (i == (len(track_vias) - 1))
            space = lib.ioringvia_pitch - track_via.width
            # Leave space at the border between vias
            width = lib.iocell_width - 2*space
            lbottom = track_via.bottom[0]
            ltop = track_via.top[0]
            for j, (bottom, top) in enumerate(subtracks_dims):
                if is_top:
                    topbottom, toptop = topsubtracks_dims[j]
                    layouter.add_wire(
                        wire=track_via, net=track,
                        **fab.spec4bound(via=track_via, bound_spec={
                            "space": space,
                            "bottom_left": -0.5*width, "bottom_right": 0.5*width,
                            "top_bottom": topbottom, "top_top": toptop,
                        }),
                    )
                    layouter.add_wire(
                        wire=lbottom, pin=lbottom.pin[0], net=track,
                        **fab.spec4bound(bound_spec={
                            "left": -0.5*lib.iocell_width,
                            "bottom": bottom,
                            "right": 0.5*lib.iocell_width,
                            "top": top,
                        }),
                    )
                    layouter.add_wire(
                        wire=ltop, pin=ltop.pin[0], net=track,
                        **fab.spec4bound(bound_spec={
                            "left": -0.5*lib.iocell_width,
                            "bottom": topbottom,
                            "right": 0.5*lib.iocell_width,
                            "top": toptop,
                        }),
                    )
                else:
                    layouter.add_wire(
                        wire=track_via, net=track,
                        **fab.spec4bound(via=track_via, bound_spec={
                            "space": space,
                            "bottom_left": -0.5*width, "bottom_right": 0.5*width,
                            "top_bottom": bottom, "top_top": top,
                        }),
                    )
                    layouter.add_wire(
                        wire=lbottom, pin=lbottom.pin[0], net=track,
                        **fab.spec4bound(bound_spec={
                            "left": -0.5*lib.iocell_width,
                            "bottom": bottom,
                            "right": 0.5*lib.iocell_width,
                            "top": top,
                        }),
                    )

        self.layout.boundary = geo.Rect.from_size(
            width=lib.iocell_width, height=height,
        )


class _IODCDuoTrackSegment(lbry._Cell):
    def __init__(self, *, name, lib, height, space):
        super().__init__(lib, name)

        ckt = self.new_circuit()
        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout

        comp = lib.computed
        metal3 = comp.metal[3].prim
        via3 = comp.vias[3]
        metal4 = comp.metal[4].prim
        via4 = comp.vias[4]
        metal5 = comp.metal[5].prim

        # vss & vdd
        track1 = ckt.new_net("track1", external=True)
        track2 = ckt.new_net("track2", external=True)

        trackheight = (height - space)/2
        trackheight = floor(trackheight/lib.tech.grid)*lib.tech.grid

        bottom1 = -0.5*height
        top1 = bottom1 + trackheight
        top2 = 0.5*height
        bottom2 = top2 - trackheight

        rect1 = geo.Rect(
            left=-0.5*lib.iocell_width, bottom=bottom1,
            right=0.5*lib.iocell_width, top=top1,
        )
        rect2 = geo.Rect(
            left=-0.5*lib.iocell_width, bottom=bottom2,
            right=0.5*lib.iocell_width, top=top2,
        )

        # metal3
        metal = metal3
        layouter.add_wire(
            net=track1, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect1),
        )
        layouter.add_wire(
            net=track2, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect2),
        )

        # metal5, rects are swapped !
        metal = metal5
        layouter.add_wire(
            net=track1, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect2),
        )
        layouter.add_wire(
            net=track2, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect1),
        )

        # metal4, reduced height
        viawidth = 0.5*(lib.iocell_width - 3*space)
        viawidth = floor(viawidth/lib.tech.grid)*lib.tech.grid
        viaheight = max(
            comp.metal[3].minwidth4ext_updown, comp.metal[4].minwidth4ext_updown,
        )

        l_via = layouter.add_wire(
            wire=via3, net=track1,
            x=lib.tech.on_grid(-0.25*lib.iocell_width + 0.5*space),
            bottom_width=viawidth, top_width=viawidth,
            y=(top1 - 0.5*viaheight), bottom_height=viaheight, top_height=viaheight,
        )
        viam4_bounds = l_via.bounds(mask=metal4.mask)
        top1b = viam4_bounds.bottom - space
        l_via = layouter.add_wire(
            wire=via3, net=track2,
            x=lib.tech.on_grid(0.25*lib.iocell_width - 0.5*space),
            bottom_width=viawidth, top_width=viawidth,
            y=(bottom2 + 0.5*viaheight), bottom_height=viaheight, top_height=viaheight,
        )
        viam4_bounds2 = l_via.bounds(mask=metal4.mask)
        bottom2b = viam4_bounds2.top + space

        metal = metal4
        rect1 = geo.Rect(
            left=-0.5*lib.iocell_width, bottom=bottom1,
            right=0.5*lib.iocell_width, top=top1b,
        )
        rect2 = geo.Rect(
            left=-0.5*lib.iocell_width, bottom=bottom2b,
            right=0.5*lib.iocell_width, top=top2,
        )
        layouter.add_wire(
            net=track1, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect2),
        )
        layouter.add_wire(
            net=track1, wire=metal, **fab.spec4bound(bound_spec={
                "left": viam4_bounds.left,
                "bottom": viam4_bounds.bottom,
                "right": viam4_bounds.right,
                "top": bottom2b + lib.tech.grid,
            })
        )
        layouter.add_wire(
            net=track2, wire=metal, pin=metal.pin[0],
            **fab.spec4bound(bound_spec=rect1),
        )
        layouter.add_wire(
            net=track2, wire=metal, **fab.spec4bound(bound_spec={
                "left": viam4_bounds2.left,
                "bottom": top1b - lib.tech.grid,
                "right": viam4_bounds2.right,
                "top": viam4_bounds2.top,
            })
        )

        # via4
        space = lib.ioringvia_pitch - via4.width
        layouter.add_wire(
            net=track1, wire=via4,
            **fab.spec4bound(via=via4, bound_spec={
                "space": space,
                "bottom_left": rect2.left + space,
                "bottom_bottom": rect2.bottom + space,
                "bottom_right": rect2.right - space,
                "bottom_top": rect2.top - space,
            }),
        )
        layouter.add_wire(
            net=track2, wire=via4,
            **fab.spec4bound(via=via4, bound_spec={
                "space": space,
                "bottom_left": rect1.left + space,
                "bottom_bottom": rect1.bottom + space,
                "bottom_right": rect1.right - space,
                "bottom_top": rect1.top - space,
            }),
        )

        layout.boundary = geo.Rect(
            left=-0.5*lib.iocell_width, bottom=bottom1,
            right=0.5*lib.iocell_width, top=top2,
        )

class _Clamp(lbry._Cell):
    def __init__(self, *, name, lib, type_, n_trans, n_drive):
        assert (
            isinstance(lib, IOLibrary)
            and type_ in ("n", "p")
            and isinstance(n_trans, int) and (n_trans > 0)
            and isinstance(n_drive, int) and (0 <= n_drive <= n_trans)
        ), "Internal error"
        tech = lib.tech
        comp = lib.computed

        cont = comp.contact
        metal1 = comp.metal[1].prim
        via1 = comp.vias[1]
        metal2 = comp.metal[2].prim
        via2 = comp.vias[2]

        super().__init__(lib, name)

        ckt = self.new_circuit()
        layouter = self.new_circuitlayouter()
        fab = layouter.fab

        iovss = ckt.new_net("iovss", external=True)
        iovdd = ckt.new_net("iovdd", external=True)
        source = iovss if type_ == "n" else iovdd
        notsource = iovdd if type_ == "n" else iovss
        pad = ckt.new_net("pad", external=True)

        gate_nets = tuple()
        gate: Optional[ckt_._CircuitNet] = None
        off: Optional[ckt_._CircuitNet] = None
        if n_drive > 0:
            gate = ckt.new_net("gate", external=True)
            gate_nets += n_drive*(gate,)
        if n_drive < n_trans:
            off = ckt.new_net("off", external=False)
            gate_nets += (n_trans - n_drive)*(off,)

        l_clamp = hlp.clamp(
            lib=lib, ckt=ckt, type_=type_,
            source_net=source, drain_net=pad, gate_nets=gate_nets,
        )
        clampact_bounds = l_clamp.bounds(mask=comp.active.mask)
        
        min_space_active_poly = tech.computed.min_space(comp.poly, comp.active)

        vspace = (
            lib.clampgate_gatecont_space + 0.5*cont.width + max(
                0.5*comp.minwidth_polywithcontact + min_space_active_poly,
                0.5*comp.metal[1].minwidth4ext_down + metal1.min_space,
            )
        )
        innerguardring_height = (
            (clampact_bounds.top - clampact_bounds.bottom) + 2*vspace
            + 2*comp.guardring_width
        )
        innerguardring_width = (lib.iocell_width - 2*comp.guardring_pitch)
        outerguardring_height = innerguardring_height + 2*comp.guardring_pitch

        c_outerring = lib.guardring(
            type_=type_, width=lib.iocell_width, height=outerguardring_height,
        )
        i_outerring = ckt.new_instance("OuterRing", c_outerring)
        notsource.childports += i_outerring.ports.conn
        c_innerring = lib.guardring(
            type_="p" if (type_ == "n") else "n",
            width=innerguardring_width, height=innerguardring_height,
            fill_well=(type_ == "p"), fill_implant=True,
        )
        i_innerring = ckt.new_instance("InnerRing", c_innerring)
        source.childports += i_innerring.ports.conn

        # Draw drain metal2 connections
        # Cache as polygons will be added during parsing
        polygons = tuple(filter(
            lambda p: p.mask == metal1.mask,
            l_clamp.net_polygons(pad),
        ))
        for polygon in polygons:
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                y = 0.5*(bounds.bottom + bounds.top)

                l_via = l_clamp.add_wire(
                    net=pad, wire=via1, x=x, columns=lib.clampdrain_via1columns,
                    **fab.spec4bound(via=via1, bound_spec={
                        "bottom_bottom": bounds.bottom,
                        "bottom_top": bounds.top,
                    }),
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)

                l_clamp.add_wire(
                    net=pad, wire=metal2, pin=metal2.pin[0],
                    **fab.spec4bound(bound_spec={
                        "left": viam2_bounds.left,
                        "bottom": viam2_bounds.bottom,
                        "right": viam2_bounds.right,
                        "top": 0.5*outerguardring_height,
                    })
                )

        # Draw IO ring tracks
        c_iotrack = lib.iotracksegment(height=outerguardring_height)
        i_iotrack = ckt.new_instance("IOTrack", c_iotrack)
        source.childports += i_iotrack.ports.track

        # Connect source to ring
        # Cache as polygons will be added during parsing
        polygons = tuple(filter(
            lambda p: p.mask == metal1.mask,
            l_clamp.net_polygons(source),
        ))
        for polygon in polygons:
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                y = 0.5*(bounds.bottom + bounds.top)

                l_via = l_clamp.add_wire(
                    net=source, wire=via1, x=x, **fab.spec4bound(
                        via=via1, bound_spec={
                            "bottom_bottom": bounds.bottom,
                            "bottom_top": bounds.top,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                
                bottom = -0.5*innerguardring_height
                top = bottom + comp.guardring_width
                l_clamp.add_wire(
                    net=source, wire=via1, x=x, **fab.spec4bound(
                        via=via1, bound_spec={
                            "bottom_bottom": bottom,
                            "bottom_top": top,
                        }
                    )
                )
                top = 0.5*innerguardring_height
                bottom = top - comp.guardring_width
                l_clamp.add_wire(
                    net=source, wire=via1, x=x, **fab.spec4bound(
                        via=via1, bound_spec={
                            "bottom_bottom": bottom,
                            "bottom_top": top,
                        }
                    )
                )
                l_clamp.add_wire(
                    net=source, wire=metal2,**fab.spec4bound(
                        bound_spec={
                            "left": viam2_bounds.left,
                            "bottom": -0.5*outerguardring_height,
                            "right": viam2_bounds.right,
                            "top": 0.5*outerguardring_height,
                        },
                    ),
                )
                l_clamp.add_wire(
                    net=source, wire=via2, x=x, **fab.spec4bound(
                        via=via2, bound_spec={
                            "bottom_bottom": -0.5*outerguardring_height,
                            "bottom_top": 0.5*outerguardring_height,
                        },
                    ),
                )

        # Place clamp, guard rings
        x = 0.5*lib.iocell_width
        y = 0.5*outerguardring_height
        layouter.place(i_outerring, x=x, y=y)
        layouter.place(i_innerring, x=x, y=y)
        l_clamp = layouter.place(l_clamp, x=x, y=y)
        l_track = layouter.place(i_iotrack, x=x, y=y)

        # Duplicate the track pins
        pinmasks = tuple(spec.prim.pin[0].mask for spec in (
            comp.metal[i] for i in range(3, 6 + 1)
        ))
        l = self.layout
        for polygon in filter(lambda p: p.mask in pinmasks, l_track.polygons):
            l.add_maskshape(net=source, maskshape=cast(geo.MaskShape, polygon))

        # Draw gate diode and connect the gates
        if n_drive > 0:
            assert gate is not None
            diode = lib.ndiode if type_ == "n" else lib.pdiode
            l_ch = fab.new_primitivelayout(
                prim=cont, portnets={"conn": gate}, rows=2,
                bottom=diode.wire, bottom_implant=diode.implant,
            )
            chact_bounds = l_ch.bounds(mask=diode.wire.mask)
            dgate = ckt.new_instance("DGATE", diode, 
                width=(chact_bounds.right - chact_bounds.left),
                height=(chact_bounds.top - chact_bounds.bottom),
            )
            an = dgate.ports.anode
            cath = dgate.ports.cathode
            gate.childports += an if type_ == "p" else cath
            source.childports += an if type_ == "n" else cath

            x = 0.5*lib.iocell_width + 0.5*(
                clampact_bounds.left - 0.5*innerguardring_width + comp.guardring_width
            )
            y = 0.5*outerguardring_height + clampact_bounds.top - chact_bounds.top
            l_ch = layouter.place(l_ch, x=x, y=y)
            layouter.place(dgate, x=x, y=y)

            chm1_bounds = l_ch.bounds(mask=metal1.mask)
            clampm1gate_bounds = l_clamp.bounds(mask=metal1.mask, net=gate)

            rect = geo.Rect.from_rect(
                rect=chm1_bounds, top=clampm1gate_bounds.top,
            )
            layouter.add_wire(
                wire=metal1, net=gate, **fab.spec4bound(bound_spec={
                    "left": rect.left, "bottom": rect.bottom,
                    "right": rect.right, "top": rect.top,
                }),
            )
            rect = geo.Rect.from_rect(
                rect=clampm1gate_bounds, left=chm1_bounds.left,
                bottom=(clampm1gate_bounds.top - metal1.min_width),
            )
            layouter.add_wire(
                wire=metal1, net=gate, **fab.spec4bound(bound_spec={
                    "left": rect.left, "bottom": rect.bottom,
                    "right": rect.right, "top": rect.top,
                }),
            )

            l_via = layouter.add_wire(
                wire=via1, net=gate, x=x,
                **fab.spec4bound(via=via1, bound_spec={
                    "bottom_bottom": chm1_bounds.bottom,
                    "bottom_top": clampm1gate_bounds.top,
                })
            )
            viam2_bounds = l_via.bounds(mask=metal2.mask)
            layouter.add_wire(
                wire=metal2, net=gate, pin=metal2.pin[0],
                **fab.spec4bound(bound_spec={
                    "left": viam2_bounds.left,
                    "bottom": viam2_bounds.bottom,
                    "right": viam2_bounds.right,
                    "top": outerguardring_height,
                })
            )

        # Draw power off resistor and connect the gates
        if n_drive < n_trans:
            assert off is not None
            roff = ckt.new_instance(
                "Roff", lib.nres if type_ == "n" else lib.pres,
                width=comp.minwidth4ext_polywithcontact,
                height=(clampact_bounds.top - clampact_bounds.bottom),
            )
            source.childports += roff.ports.port1
            off.childports += roff.ports.port2

            x = 0.5*lib.iocell_width + 0.5*(
                clampact_bounds.right + 0.5*innerguardring_width - comp.guardring_width
            )
            y = 0.5*outerguardring_height
            l_roff = layouter.place(roff, x=x, y=y)
            roffm1source_bounds = l_roff.bounds(mask=metal1.mask, net=source)
            roffm1off_bounds = l_roff.bounds(mask=metal1.mask, net=off)

            x = 0.5*(roffm1source_bounds.left + roffm1source_bounds.right)
            y = 0.5*(roffm1source_bounds.bottom + roffm1source_bounds.top)
            l_via = layouter.add_wire(
                wire=via1, net=source, x=x, y=y, columns=2,
            )
            viam2_bounds = l_via.bounds(mask=metal2.mask)
            y = comp.guardring_pitch + 0.5*comp.guardring_width
            l_via = layouter.add_wire(
                wire=via1, net=source, x=x, y=y, columns=2,
            )
            viam2_bounds2 = l_via.bounds(mask=metal2.mask)
            layouter.add_wire(
                wire=metal2, net=source, **fab.spec4bound(bound_spec={
                    "left": viam2_bounds.left,
                    "bottom": viam2_bounds2.bottom,
                    "right": viam2_bounds.right,
                    "top": viam2_bounds.top,
                }),
            )

            clampm1off_bounds = l_clamp.bounds(mask=metal1.mask, net=off)
            roffm1off_bounds = l_roff.bounds(mask=metal1.mask, net=off)

            rect = geo.Rect.from_rect(
                rect=roffm1off_bounds, top=clampm1off_bounds.top,
            )
            layouter.add_wire(
                wire=metal1, net=off, **fab.spec4bound(bound_spec={
                    "left": rect.left, "bottom": rect.bottom,
                    "right": rect.right, "top": rect.top,
                }),
            )
            layouter.add_wire(
                wire=metal1, net=off, **fab.spec4bound(bound_spec={
                    "left": clampm1off_bounds.left,
                    "bottom": clampm1off_bounds.top - metal1.min_width,
                    "right": roffm1off_bounds.right,
                    "top": clampm1off_bounds.top,
                }),
            )

        self.layout.boundary = geo.Rect(
            left=0.0, bottom=0.0,
            right=lib.iocell_width, top=outerguardring_height,
        )


class _Secondary(lbry._Cell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"
        comp = lib.computed

        cont = comp.contact
        metal1 = comp.metal[1].prim
        via1 = comp.vias[1]
        metal2 = comp.metal[2].prim
        metal2pin = metal2.pin[0]

        super().__init__(lib, "SecondaryProtection")

        ckt = self.new_circuit()
        layouter = self.new_circuitlayouter()
        layout = layouter.layout

        iovdd = ckt.new_net("iovdd", external=True)
        iovss = ckt.new_net("iovss", external=True)
        pad = ckt.new_net("pad", external=True)
        core = ckt.new_net("core", external=True)

        # Resistance
        r = ckt.new_instance(
            "R", lib.nres,
            width=lib.secondres_width, height=lib.secondres_length,
        )
        pad.childports += r.ports.port1
        core.childports += r.ports.port2

        l_res = layouter.inst_layout(r)
        respoly_bounds = l_res.bounds(mask=comp.poly.mask)
        res_width = respoly_bounds.right - respoly_bounds.left
        res_height = respoly_bounds.top - respoly_bounds.bottom

        guard_width = (
            res_width + 2*lib.secondres_active_space + 2*comp.guardring_width
        )
        guard_height = (
            res_height + 2*lib.secondres_active_space + 2*comp.guardring_width
        )
        l_guard = hlp.guardring(
            lib=lib, net=iovss, type_="p", width=guard_width, height=guard_height,
        )

        l_res = layouter.place(l_res, x=0.5*guard_width, y=0.5*guard_height)
        resm1pad_bounds = l_res.bounds(mask=metal1.mask, net=pad)
        resm1core_bounds = l_res.bounds(mask=metal1.mask, net=core)
        l_guard = layouter.place(l_guard, x=0.5*guard_width, y=0.5*guard_height)
        resm1guard_bounds = l_guard.bounds(mask=metal1.mask)

        # N diode
        dn_prim = lib.ndiode
        diode_height = (
            guard_height - 2*comp.guardring_width - 2*comp.min_space_nmos_active
        )
        l_ch = layouter.fab.new_primitivelayout(
            cont, portnets={"conn": core}, columns=2,
            bottom=dn_prim.wire, bottom_implant=dn_prim.implant,
            bottom_height=diode_height,
        )
        dnchact_bounds = l_ch.bounds(mask=comp.active.mask)
        diode_width = dnchact_bounds.right - dnchact_bounds.left
        dn = ckt.new_instance(
            "DN", dn_prim, width=diode_width, height=diode_height,
        )
        core.childports += dn.ports.cathode
        iovss.childports += dn.ports.anode

        guard2_width = (
            diode_width + 2*comp.min_space_nmos_active + 2*comp.guardring_width
        )
        l_guard2 = hlp.guardring(
            lib=lib, net=iovss, type_="p", width=guard2_width, height=guard_height,
        )

        x = guard_width + comp.guardring_space + 0.5*guard2_width
        y = 0.5*guard_height
        l_dn_ch = layouter.place(l_ch, x=x, y=y)
        layouter.place(dn, x=x, y=y)
        l_guard2 = layouter.place(l_guard2, x=x, y=y)
        ndiom1guard_bounds = l_guard2.bounds(mask=metal1.mask)

        # connect guard rings
        # currently can't be done in metal1 as no shapes with two or more holes
        # in it are supported.
        _l_via = layouter.fab.new_primitivelayout(
            via1, portnets={"conn": iovss}, rows=3,
        )
        _m1_bounds = _l_via.bounds(mask=metal1.mask)
        y = (
            max(resm1guard_bounds.bottom, ndiom1guard_bounds.bottom)
            - _m1_bounds.bottom + 2*metal2.min_space
        )
        l = layouter.place(
            _l_via, x=(resm1guard_bounds.right - _m1_bounds.right), y=y,
        )
        m2_bounds1 = l.bounds(mask=metal2.mask)
        l = layouter.place(
            _l_via, x=(ndiom1guard_bounds.left - _m1_bounds.left), y=y,
        )
        m2_bounds2 = l.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=iovss, wire=metal2, pin=metal2pin,
            **layouter.fab.spec4bound(bound_spec={
                "left": m2_bounds1.left,
                "bottom": m2_bounds1.bottom,
                "right": m2_bounds2.right,
                "top": m2_bounds2.top,
            }),
        )

        # P diode
        dp_prim = lib.pdiode
        guard3_width = (
            guard_width + comp.guardring_space + guard2_width
        )
        diode2_width = (
            guard3_width - 2*comp.guardring_width - 2*comp.min_space_pmos_active
        )
        l_ch = layouter.fab.new_primitivelayout(
            cont, portnets={"conn": core}, rows=2,
            bottom=dp_prim.wire, bottom_implant=dp_prim.implant,
            bottom_width=diode2_width,
        )
        dpchact_bounds = l_ch.bounds(mask=comp.active.mask)
        diode2_height = dpchact_bounds.top - dpchact_bounds.bottom
        dp = ckt.new_instance(
            "DP", dp_prim, width=diode2_width, height=diode2_height,
        )
        core.childports += dp.ports.anode
        iovdd.childports += dp.ports.cathode

        guard3_height = (
            diode2_height + 2*comp.min_space_pmos_active + 2*comp.guardring_width
        )
        l_guard3 = hlp.guardring(
            lib=lib, net=iovdd, type_="n", width=guard3_width, height=guard3_height,
        )

        x = 0.5*guard3_width
        y = guard_height + comp.guardring_space + 0.5*guard3_height
        l_dp_ch = layouter.place(l_ch, x=x, y=y)
        layouter.place(dp, x=x, y=y)
        l_guard3 = layouter.place(l_guard3, x=x, y=y)

        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0,
            right=(guard_width + comp.guardring_space + guard2_width),
            top=(guard_height + comp.guardring_space + guard3_height),
        )

        # Draw interconnects
        w = resm1pad_bounds.right - resm1pad_bounds.left
        x = 0.5*(resm1pad_bounds.left + resm1pad_bounds.right)
        l_via = layouter.fab.new_primitivelayout(
            via1, portnets={"conn": pad}, bottom_width=w,
        )
        viam1_bounds = l_via.bounds(mask=metal1.mask)
        y = -viam1_bounds.top + resm1pad_bounds.top
        l_via = layouter.place(l_via, x=x, y=y)
        resm2pad_bounds = l_via.bounds(mask=metal2.mask)
        layouter.add_wire(
            wire=metal2, net=pad, pin=metal2.pin[0],
            **layouter.fab.spec4bound(bound_spec={
                "left": resm1pad_bounds.left,
                "bottom": 0.0,
                "right": resm1pad_bounds.right,
                "top": resm2pad_bounds.top,
            }),
        )

        dnm1_bounds = l_dn_ch.bounds(mask=metal1.mask)
        dpm1_bounds = l_dp_ch.bounds(mask=metal1.mask)

        w = resm1core_bounds.right - resm1core_bounds.left
        x = 0.5*(resm1core_bounds.left + resm1core_bounds.right)
        l_via = layouter.fab.new_primitivelayout(
            via1, portnets={"conn": core}, bottom_width=w
        )
        viam1_bounds = l_via.bounds(mask=metal1.mask)
        y = -viam1_bounds.bottom + resm1core_bounds.bottom
        layouter.place(l_via, x=x, y=y)
        layouter.add_wire(
            wire=metal2, net=core,
            **layouter.fab.spec4bound(bound_spec={
                "left": resm1core_bounds.left,
                "bottom": resm1core_bounds.bottom,
                "right": resm1core_bounds.right,
                "top": dpm1_bounds.top,
            }),
        )

        w = dnm1_bounds.right - dnm1_bounds.left
        h = dnm1_bounds.top - dnm1_bounds.bottom
        x = 0.5*(dnm1_bounds.left + dnm1_bounds.right)
        y = 0.5*(dnm1_bounds.bottom + dnm1_bounds.top)
        layouter.add_wire(
            wire=via1, net=core, x=x, y=y, bottom_width=w, bottom_height=h,
        )
        layouter.add_wire(
            wire=metal2, net=core,
            **layouter.fab.spec4bound(bound_spec={
                "left": dnm1_bounds.left,
                "bottom": dnm1_bounds.bottom,
                "right": dnm1_bounds.right,
                "top": dpm1_bounds.top,
            }),
        )

        w = dpm1_bounds.right - dpm1_bounds.left
        h = dpm1_bounds.top - dpm1_bounds.bottom
        x = 0.5*(dpm1_bounds.left + dpm1_bounds.right)
        y = 0.5*(dpm1_bounds.bottom + dpm1_bounds.top)
        layouter.add_wire(
            wire=via1, net=core, x=x, y=y, bottom_width=w, bottom_height=h,
        )
        layouter.add_wire(
            wire=metal2, net=core, pin=metal2.pin[0],
            **layouter.fab.spec4bound(bound_spec={
                "left": dpm1_bounds.left,
                "bottom": dpm1_bounds.bottom,
                "right": dpm1_bounds.right,
                "top": layout.boundary.top,
            }),
        )


class _Pad(lbry._OnDemandCell):
    def __init__(self, *, name, lib, width, height):
        assert isinstance(lib, IOLibrary), "Internal error"
        super().__init__(lib, name)

        self.width = width
        self.height = height

    def _create_circuit(self):
        ckt = self.new_circuit()
        ckt.new_net("pad", external=True)

    def _create_layout(self):
        lib = self.lib
        width = self.width
        height = self.height

        comp = lib.computed
        pad_net = self.circuit.nets.pad
        layouter = self.new_circuitlayouter()
        layout = layouter.layout


        enc = comp.pad.min_bottom_enclosure.max()
        metal_width = width + 2*enc
        metal_height = height + 2*enc

        metal_bounds = geo.Rect.from_size(width=metal_width, height=metal_height)
        pad_bounds = geo.Rect.from_size(width=width, height=height)

        # TODO: add support in layouter.add_wire for PadOpening
        layouter.add_shape(prim=comp.pad.bottom, net=pad_net, shape=metal_bounds)
        layouter.add_shape(prim=comp.pad.bottom.pin[0], net=pad_net, shape=pad_bounds)
        layouter.add_shape(prim=comp.pad, net=pad_net, shape=pad_bounds)

        for i, via in enumerate(comp.vias[1:]):
            l_via = hlp.diamondvia(
                lib=lib, net=pad_net, via=via,
                width=metal_width, height=metal_height,
                space=(lib.padvia_pitch - via.width),
                enclosure=prp.Enclosure(lib.padvia_metal_enclosure),
                center_via=((i%2) == 0), corner_distance=lib.padvia_corner_distance,
            )
            layouter.place(l_via, x=0.0, y=0.0)

        layout.boundary = metal_bounds


class _LevelUp(lbry._Cell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "LevelUp")

        self._add_circuit()
        self._add_layout()

    def _add_circuit(self):
        lib = self.lib

        circuit = self.new_circuit()

        iopmos_lvlshift_w = max(
            lib.iopmos.computed.min_w, lib.computed.minwidth4ext_activewithcontact,
        )

        # Input inverter
        n_i_inv = circuit.new_instance("n_i_inv", lib.nmos, w=lib.computed.maxnmos_w)
        p_i_inv = circuit.new_instance("p_i_inv", lib.pmos, w=lib.computed.maxpmos_w)

        # Level shifter
        n_lvld_n = circuit.new_instance(
            "n_lvld_n", lib.ionmos, w=lib.computed.maxionmos_w,
            sd_width=0.5*lib.ionmos.computed.min_gate_space,
        )
        n_lvld = circuit.new_instance(
            "n_lvld", lib.ionmos, w=lib.computed.maxionmos_w,
            sd_width=0.5*lib.ionmos.computed.min_gate_space,
        )
        p_lvld_n = circuit.new_instance(
            "p_lvld_n", lib.iopmos, w=iopmos_lvlshift_w,
            sd_width=0.5*lib.iopmos.computed.min_gate_space,
        )
        p_lvld = circuit.new_instance(
            "p_lvld", lib.iopmos, w=iopmos_lvlshift_w,
            sd_width=0.5*lib.iopmos.computed.min_gate_space,
        )

        # Output inverter/driver
        n_lvld_n_inv = circuit.new_instance(
            "n_lvld_n_inv", lib.ionmos, w=lib.computed.maxionmos_w,
        )
        p_lvld_n_inv = circuit.new_instance(
            "p_lvld_n_inv", lib.iopmos, w=lib.computed.maxiopmos_w,
        )

        # Create the nets
        circuit.new_net("vdd", external=True, childports=(
            p_i_inv.ports.sourcedrain2, p_i_inv.ports.bulk,
        ))
        circuit.new_net("iovdd", external=True, childports=(
            p_lvld_n.ports.sourcedrain1, p_lvld_n.ports.bulk,
            p_lvld.ports.sourcedrain2, p_lvld.ports.bulk,
            p_lvld_n_inv.ports.sourcedrain1, p_lvld_n_inv.ports.bulk,
        ))
        circuit.new_net("vss", external=True, childports=(
            n_i_inv.ports.sourcedrain2, n_i_inv.ports.bulk,
            n_lvld_n.ports.sourcedrain1, n_lvld_n.ports.bulk,
            n_lvld.ports.sourcedrain2, n_lvld.ports.bulk,
            n_lvld_n_inv.ports.sourcedrain1, n_lvld_n_inv.ports.bulk,
        ))

        circuit.new_net("i", external=True, childports=(
            n_i_inv.ports.gate, p_i_inv.ports.gate,
            n_lvld_n.ports.gate,
        ))
        circuit.new_net("i_n", external=False, childports=(
            n_i_inv.ports.sourcedrain1, p_i_inv.ports.sourcedrain1,
            n_lvld.ports.gate,
        ))
        circuit.new_net("lvld", external=False, childports=(
            p_lvld_n.ports.gate,
            n_lvld.ports.sourcedrain1, p_lvld.ports.sourcedrain1,
        ))
        circuit.new_net("lvld_n", external=False, childports=(
            n_lvld_n.ports.sourcedrain2, p_lvld_n.ports.sourcedrain2,
            p_lvld.ports.gate,
            n_lvld_n_inv.ports.gate, p_lvld_n_inv.ports.gate,
        ))
        circuit.new_net("o", external=True, childports=(
            n_lvld_n_inv.ports.sourcedrain2, p_lvld_n_inv.ports.sourcedrain2,
        ))

    def _add_layout(self):
        lib = self.lib
        comp = lib.computed

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        active = comp.active
        nimplant = comp.nimplant
        pimplant = comp.pimplant
        ionimplant = comp.ionimplant
        iopimplant = comp.iopimplant
        assert pimplant == iopimplant
        nwell = comp.nwell
        poly = comp.poly
        contact = comp.contact
        metal1 = contact.top[0]
        metal1pin = metal1.pin[0]
        via1 = comp.vias[1]
        metal2 = via1.top[0]
        metal2pin = metal2.pin[0]

        chm1_enc = contact.min_top_enclosure[0]
        chm1_wideenc = prp.Enclosure((chm1_enc.max(), chm1_enc.min()))

        actox_enc = (
            comp.activeoxide_enclosure.max() if comp.activeoxide_enclosure is not None
            else comp.iogateoxide_enclosure.max()
        )

        iopmos_lvlshift_w = max(
            lib.iopmos.computed.min_w, comp.minwidth4ext_activewithcontact,
        )

        layouter = self.new_circuitlayouter()
        layout = self.layout
        active_left = 0.5*active.min_space

        if metal2.pin is not None:
            assert len(metal2.pin) == 1
            pin_args = {"pin": metal2pin}
        else:
            pin_args = {}

        #
        # Core row
        #

        x = active_left + 0.5*comp.minwidth_activewithcontact
        chbounds_n = layouter.add_wire(
            net=nets.i_n, wire=contact, x=x,
            bottom=active, bottom_implant=nimplant,
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxnmos_activebottom,
                    "bottom_top": comp.maxnmos_activetop,
                }
            ),
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.i_n, wire=active, implant=nimplant,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxnmos_y, height=comp.maxnmos_w,
        )
        chbounds_p = layouter.add_wire(
            net=nets.i_n, well_net=nets.vdd, wire=contact, x=x,
            bottom=active, bottom_implant=pimplant, bottom_well=nwell,
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxpmos_activebottom,
                    "bottom_top": comp.maxpmos_activetop,
                }
            )
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.i_n, well_net=nets.vdd, wire=active, implant=pimplant, well=nwell,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxpmos_y, height=comp.maxpmos_w,
        )
        layouter.add_wire(
            net=nets.i_n, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": chbounds_n.left, "bottom": chbounds_n.top,
                "right": chbounds_n.right, "top": chbounds_p.bottom,
            }),
        )
        m2bounds = layouter.add_wire(
            net=nets.i_n, wire=via1, **lib.layoutfab.spec4bound(
                via=via1, bound_spec={
                    "bottom_bottom": chbounds_n.bottom,
                    "bottom_top": chbounds_p.top,
                    "bottom_right": chbounds_n.right,
                    "bottom_left":
                        chbounds_n.right - comp.metal[1].minwidth_up,
                },
            )
        ).bounds(mask=metal2.mask)
        i_n_m2bounds = geo.Rect.from_rect(rect=m2bounds, bottom=lib.iorow_height)
        layouter.add_wire(
            net=nets.i_n, wire=metal2,
            **lib.layoutfab.spec4bound(bound_spec=i_n_m2bounds),
        )

        x += max((
            comp.minnmos_contactgatepitch,
            comp.minpmos_contactgatepitch,
        ))
        gatebounds_n = layouter.place(
            insts.n_i_inv, x=x, y=comp.maxnmos_y,
        ).bounds(mask=poly.mask)
        gatebounds_p = layouter.place(
            insts.p_i_inv, x=x, y=comp.maxpmos_y,
        ).bounds(mask=poly.mask)

        left = min(gatebounds_n.left, gatebounds_p.left)
        bottom = gatebounds_n.top
        right = max(gatebounds_n.right, gatebounds_p.right)
        top = gatebounds_p.bottom
        polybounds = layouter.add_wire(
            net=nets.i, wire=poly,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": left, "bottom": bottom, "right": right, "top": top,
            })
        ).bounds(mask=poly.mask)
        x_pad = max(
            polybounds.left + 0.5*comp.minwidth4ext_polywithcontact,
            chbounds_n.right + metal1.min_space
            + 0.5*comp.metal[1].minwidth4ext_down,
        )
        l = layouter.add_wire(
            net=nets.i, wire=contact, bottom=poly, x=x_pad,
            y=0.5*(polybounds.bottom + polybounds.top),
        )
        gatebounds = l.bounds(mask=metal1.mask)
        polybounds2 = l.bounds(mask=poly.mask)
        if polybounds2.left > polybounds.right:
            layouter.add_wire(
                net=nets.i, wire=poly,
                **lib.layoutfab.spec4bound(bound_spec={
                    "left": polybounds.left, "bottom": polybounds2.bottom,
                    "right": polybounds2.right, "top": polybounds2.top,
                }),
            )

        x += max((
            comp.minnmos_contactgatepitch,
            comp.minpmos_contactgatepitch,
        ))
        chbounds_n = layouter.add_wire(
            net=nets.vss, wire=contact, x=x,
            bottom=active, bottom_implant=comp.nmos.implant[0],
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxnmos_activebottom,
                    "bottom_top": comp.maxnmos_activetop,
                    "top_top": gatebounds.bottom - metal1.min_width,
                }
            ),
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vss, wire=active, implant=nimplant,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxnmos_y, height=comp.maxnmos_w,
        )
        chbounds_p = layouter.add_wire(
            net=nets.vdd, wire=contact, x=x,
            bottom=active, bottom_implant=comp.pmos.implant[0],
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxpmos_activebottom,
                    "bottom_top": comp.maxpmos_activetop,
                    "top_bottom": gatebounds.top + metal1.min_width,
                }
            )
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vdd, well_net=nets.vdd, wire=active, implant=pimplant, well=nwell,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxpmos_y, height=comp.maxpmos_w,
        )
        
        layouter.add_wire(
            net=nets.vss, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": chbounds_n.left, "bottom": lib.iorow_height,
                "right": chbounds_n.right, "top": chbounds_n.bottom,
            }),
        )
        layouter.add_wire(
            net=nets.vdd, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": chbounds_p.left, "bottom": chbounds_p.top,
                "right": chbounds_p.right, "top": lib.cells_height,
            }),
        )

        x += comp.minwidth_activewithcontact + active.min_space
        chbounds_ndio = layouter.add_wire(
            net=nets.i, wire=contact, x=x,
            bottom=active, bottom_implant=comp.nmos.implant[0],
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxnmos_activebottom,
                    "bottom_top": comp.maxnmos_activetop,
                }
            ),
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.i, wire=active, implant=nimplant,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxnmos_y, height=comp.maxnmos_w,
        )
        chbounds_pdio = layouter.add_wire(
            net=nets.i, wire=contact, x=x,
            bottom=active, bottom_implant=comp.pmos.implant[0],
            **lib.layoutfab.spec4bound(
                via=contact, bound_spec={
                    "bottom_bottom": comp.maxpmos_activebottom,
                    "bottom_top": comp.maxpmos_activetop,
                }
            )
        ).bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.i, well_net=nets.vdd, wire=active, implant=pimplant, well=nwell,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxpmos_y, height=comp.maxpmos_w,
        )
        layouter.add_wire(
            net=nets.i, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": chbounds_ndio.left, "bottom": chbounds_ndio.top,
                "right": chbounds_ndio.right, "top": chbounds_pdio.bottom,
            }),
        )
        layouter.add_wire(
            net=nets.i, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": gatebounds.left,
                "bottom": chbounds_n.top + metal1.min_width,
                "right": chbounds_ndio.right,
                "top": chbounds_p.bottom - metal1.min_width,
            }),
        )
        i_m2bounds = layouter.add_wire(
            net=nets.i, wire=via1, x=x,
            **lib.layoutfab.spec4bound(via=via1, bound_spec={
                "bottom_bottom": chbounds_ndio.bottom,
                "bottom_top": chbounds_pdio.top,
            })
        ).bounds(mask=metal2.mask)
        layouter.add_wire(
            net=nets.i, wire=metal2, **pin_args,
            **lib.layoutfab.spec4bound(bound_spec=i_m2bounds),
        )

        #
        # IO row
        #

        chtrans_pitch = max(
            comp.minionmos_contactgatepitch,
            comp.miniopmos_contactgatepitch,
        )

        x_ch1 = active_left + 0.5*comp.minwidth_activewithcontact
        x_trans1 = x_ch1 + chtrans_pitch
        x_ch2 = x_trans1 + chtrans_pitch
        x_trans2 = x_ch2 + chtrans_pitch
        x_ch3 = x_trans2 + chtrans_pitch

        # # Place n_lvld and p_lvld, and compute pad y placements
        l = layouter.place(insts.n_lvld, x=x_trans1, y=comp.maxionmos_y)
        n_lvld_polybounds = l.bounds(mask=poly.mask)
        n_lvld_pad_top = (
            comp.maxionmos_activebottom
            - lib.tech.computed.min_space(active, poly)
        )
        n_lvld_pad_bottom = n_lvld_pad_top - comp.minwidth4ext_polywithcontact
        # y_iopmos_lvlshift = (
        #     p_lvld_pad_bottom
        #     - lib.tech.computed.min_space(active, poly)
        #     - 0.5*insts.p_lvld.params["w"]
        # )
        y_iopmos_lvlshift = comp.maxiopmos_y
        l = layouter.place(insts.p_lvld, x=x_trans1, y=y_iopmos_lvlshift)
        p_lvld_pimplbounds = l.bounds(mask=iopimplant.mask)
        pimpl_bottom = p_lvld_pimplbounds.bottom
        pimpl_top = p_lvld_pimplbounds.top
        p_lvld_polybounds = l.bounds(mask=poly.mask)
        w = cast(ckt_._PrimitiveInstance, insts.p_lvld).params["w"]
        p_lvld_pad_bottom = (
            y_iopmos_lvlshift + 0.5*w
            + lib.tech.computed.min_space(active, poly)
        )
        p_lvld_pad_top = (
            p_lvld_pad_bottom + comp.minwidth4ext_polywithcontact
        )
        p_lvld_n_pad_bottom = p_lvld_pad_top + poly.min_space
        p_lvld_n_pad_top = p_lvld_n_pad_bottom + comp.minwidth4ext_polywithcontact
        # Place n_lvld_n and p_lvld_n
        l = layouter.place(insts.n_lvld_n, x=x_trans2, y=comp.maxionmos_y)
        n_lvld_n_polybounds = l.bounds(mask=poly.mask)
        l = layouter.place(insts.p_lvld_n, x=x_trans2, y=y_iopmos_lvlshift)
        p_lvld_n_polybounds = l.bounds(mask=poly.mask)

        # Place left source-drain contacts
        l = layouter.add_wire(
            net=nets.lvld, wire=contact, x=x_ch1,
            y=comp.maxionmos_y, bottom_height=comp.maxionmos_w,
            bottom=active, bottom_implant=ionimplant,
        )
        nch_lvld_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.lvld, wire=active, implant=ionimplant,
            x=x_ch1, width=comp.minwidth_activewithcontact,
            y=comp.maxionmos_y, height=comp.maxionmos_w,
        )
        l = layouter.add_wire(
            net=nets.lvld, well_net=nets.iovdd, wire=contact, x=x_ch1,
            y=y_iopmos_lvlshift, bottom_height=iopmos_lvlshift_w,
            bottom=active, bottom_implant=iopimplant, bottom_well=nwell,
        )
        pch_lvld_m1bounds = l.bounds(mask=metal1.mask)
        pimpl_left = l.bounds(mask=iopimplant.mask).left
        layouter.add_wire(
            net=nets.lvld, well_net=nets.iovdd, wire=active,
            implant=iopimplant, well=nwell,
            x=x_ch1, width=comp.minwidth_activewithcontact,
            y=y_iopmos_lvlshift, height=iopmos_lvlshift_w,
        )
        layouter.add_wire(
            net=nets.lvld, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": nch_lvld_m1bounds.left,
                "bottom": pch_lvld_m1bounds.top,
                "right": nch_lvld_m1bounds.right,
                "top": nch_lvld_m1bounds.bottom,
            }),
        )

        # Poly pads for nmoses of the level shifter
        right = x_ch2 - 0.5*poly.min_space
        left = right - comp.minwidth_polywithcontact
        if left > n_lvld_polybounds.left:
            left = n_lvld_polybounds.left
            right = left + comp.minwidth_polywithcontact
            right2 = n_lvld_n_polybounds.right
            left2 = right2 - comp.minwidth_polywithcontact
        else:
            left2 = right + poly.min_space
            right2 = left2 + comp.minwidth_polywithcontact
        l = layouter.add_wire(
            net=nets.i_n, wire=contact, bottom=poly,
            **lib.layoutfab.spec4bound(via=contact, bound_spec={
                "bottom_left": left,
                "bottom_bottom": n_lvld_pad_bottom,
                "bottom_right": right,
                "bottom_top": n_lvld_pad_top,
            })
        )
        # n_lvld_n_pad_polybounds = l.bounds(mask=poly.mask)
        n_lvld_n_pad_m1bounds = l.bounds(mask=metal1.mask)
        l = layouter.add_wire(
            net=nets.i, wire=contact, bottom=poly,
            **lib.layoutfab.spec4bound(via=contact, bound_spec={
                "bottom_left": left2,
                "bottom_bottom": n_lvld_pad_bottom,
                "bottom_right": right2,
                "bottom_top": n_lvld_pad_top,
            })
        )
        # n_lvld_pad_polybounds = l.bounds(mask=poly.mask)
        # via1 pads on the poly pads
        right = x_ch2 - 0.5*metal2.min_space
        left = right - comp.metal[2].minwidth4ext_down
        left2 = right + metal2.min_space
        right2 = left2 + comp.metal[2].minwidth4ext_down
        top = n_lvld_n_pad_m1bounds.top
        # draw two vias to make sure metal1 area is big enough
        bottom = (
            top - comp.metal[1].minwidth4ext_up
            - via1.width - via1.min_space
        )
        l = layouter.add_wire(
            net=nets.i_n, wire=via1,
            **lib.layoutfab.spec4bound(via=via1, bound_spec={
                "top_left": left,
                "bottom_bottom": bottom,
                "top_right": right,
                "bottom_top": top,
            })
        )
        n_lvld_pad_m1bounds = l.bounds(mask=metal1.mask)
        assert n_lvld_pad_m1bounds.left >= (
            nch_lvld_m1bounds.right + metal1.min_space
        )
        n_lvld_pad_m2bounds = l.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=nets.i_n, wire=metal2,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": i_n_m2bounds.left, "bottom": n_lvld_pad_m2bounds.bottom,
                "right": i_n_m2bounds.right, "top": i_n_m2bounds.bottom,
            }),
        )
        layouter.add_wire(
            net=nets.i_n, wire=metal2,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": i_n_m2bounds.left, "bottom": n_lvld_pad_m2bounds.bottom,
                "right": n_lvld_pad_m2bounds.right, "top": n_lvld_pad_m2bounds.top,
            }),
        )
        l = layouter.add_wire(
            net=nets.i, wire=via1,
            **lib.layoutfab.spec4bound(via=via1, bound_spec={
                "top_left": left2,
                "bottom_bottom": bottom,
                "top_right": right2,
                "bottom_top": top,
            })
        )
        n_lvld_pad_m1bounds = l.bounds(mask=metal1.mask)
        n_lvld_pad_m2bounds = l.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=nets.i, wire=metal2,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": i_m2bounds.left, "bottom": n_lvld_pad_m2bounds.bottom,
                "right": i_m2bounds.right, "top": i_m2bounds.bottom,
            }),
        )
        assert i_m2bounds.left <= n_lvld_pad_m2bounds.right

        # Poly pads for the pmoses of the level shifter
        left = p_lvld_polybounds.left
        right = left + comp.minwidth_polywithcontact
        l = layouter.add_wire(
            net=nets.lvld, wire=contact, bottom=poly,
            **lib.layoutfab.spec4bound(via=contact, bound_spec={
                "bottom_left": left,
                "bottom_bottom": p_lvld_pad_bottom,
                "bottom_right": right,
                "bottom_top": p_lvld_pad_top,
            })
        )
        p_lvld_pad_m1bounds = l.bounds(mask=metal1.mask)
        right = p_lvld_n_polybounds.right
        left = right - comp.minwidth_polywithcontact
        l = layouter.add_wire(
            net=nets.lvld_n, wire=contact, bottom=poly,
            **lib.layoutfab.spec4bound(via=contact, bound_spec={
                "bottom_left": left,
                "bottom_bottom": p_lvld_n_pad_bottom,
                "bottom_right": right,
                "bottom_top": p_lvld_n_pad_top,
            })
        )
        p_lvld_n_pad_polybounds = l.bounds(mask=poly.mask)
        layouter.add_wire(
            net=nets.lvld, wire=poly,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": p_lvld_n_polybounds.left,
                "bottom": p_lvld_n_polybounds.top,
                "right": p_lvld_n_polybounds.right,
                "top": p_lvld_n_pad_polybounds.top,
            }),
        )
        p_lvld_n_pad_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.lvld, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": pch_lvld_m1bounds.left,
                "bottom": p_lvld_n_pad_m1bounds.bottom,
                "right": p_lvld_n_pad_m1bounds.right,
                "top": p_lvld_n_pad_m1bounds.top,
            }),
        )

        # Place mid source-drain contacts
        # TODO: Fix bug that does not allow to add transistor after the contact
        l = layouter.add_wire(
            net=nets.vss, wire=contact, x=x_ch2,
            y=comp.maxionmos_y, bottom_height=comp.maxionmos_w,
            bottom=active, bottom_implant=ionimplant,
        )
        lvlshift_vss_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vss, wire=active, implant=ionimplant,
            x=x_ch2, width=comp.minwidth_activewithcontact,
            y=comp.maxionmos_y, height=comp.maxionmos_w,
        )
        layouter.add_wire(
            net=nets.vss, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": lvlshift_vss_m1bounds.left,
                "bottom": lvlshift_vss_m1bounds.top,
                "right": lvlshift_vss_m1bounds.right,
                "top": lib.iorow_height,
            }),
        )
        l = layouter.add_wire(
            net=nets.iovdd, well_net=nets.iovdd, wire=contact, x=x_ch2,
            y=y_iopmos_lvlshift, bottom_height=iopmos_lvlshift_w,
            bottom=active, bottom_implant=iopimplant, bottom_well=nwell,
            top_enclosure=chm1_wideenc,
        )
        lvlshift_iovdd_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.iovdd, well_net=nets.iovdd, wire=active, implant=iopimplant, well=nwell,
            x=x_ch2, width=comp.minwidth_activewithcontact,
            y=y_iopmos_lvlshift, height=iopmos_lvlshift_w,
        )
        layouter.add_wire(
            net=nets.iovdd, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": lvlshift_iovdd_m1bounds.left,
                "bottom": 0.0,
                "right": lvlshift_iovdd_m1bounds.right,
                "top": lvlshift_iovdd_m1bounds.bottom,
            }),
        )

        # Place right source-drain contacts
        l = layouter.add_wire(
            net=nets.lvld_n, wire=contact, x=x_ch3,
            y=comp.maxionmos_y, bottom_height=comp.maxionmos_w,
            bottom=active, bottom_implant=ionimplant,
        )
        nch_lvld_n_m1bounds = l.bounds(mask=metal1.mask)
        assert nch_lvld_n_m1bounds.left >= (
            n_lvld_pad_m1bounds.right + metal1.min_space
        )
        layouter.add_wire(
            net=nets.lvld_n, wire=active, implant=ionimplant,
            x=x_ch3, width=comp.minwidth_activewithcontact,
            y=comp.maxionmos_y, height=comp.maxionmos_w,
        )
        l = layouter.add_wire(
            net=nets.lvld_n, well_net=nets.iovdd, wire=contact, x=x_ch3,
            y=y_iopmos_lvlshift, bottom_height=iopmos_lvlshift_w,
            bottom=active, bottom_implant=iopimplant, bottom_well=nwell,
        )
        pch_lvld_n_m1bounds = l.bounds(mask=metal1.mask)
        pimpl_right = l.bounds(mask=iopimplant.mask).right
        layouter.add_wire(
            net=nets.lvld_n, well_net=nets.iovdd, wire=active, implant=iopimplant, well=nwell,
            x=x_ch3, width=comp.minwidth_activewithcontact,
            y=y_iopmos_lvlshift, height=iopmos_lvlshift_w,
        )
        layouter.add_wire(
            net=nets.lvld_n, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": nch_lvld_n_m1bounds.left,
                "bottom": pch_lvld_n_m1bounds.top,
                "right": nch_lvld_n_m1bounds.right,
                "top": nch_lvld_n_m1bounds.bottom,
            }),
        )
        layouter.add_wire(
            net=nets.lvld_n, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": p_lvld_pad_m1bounds.right,
                "bottom": p_lvld_pad_m1bounds.bottom,
                "right": pch_lvld_n_m1bounds.left,
                "top": p_lvld_pad_m1bounds.top,
            }),
        )
        # Add manual implant rectangle
        layouter.add_shape(prim=iopimplant, net=None, shape=geo.Rect(
            left=pimpl_left, bottom=pimpl_bottom,
            right=pimpl_right, top=pimpl_top,
        ))

        # Output buffer
        x = x_ch3 + comp.minwidth_activewithcontact + comp.active.min_space

        # Place left source-drain contacts
        l = layouter.add_wire(
            net=nets.vss, wire=contact, x=x,
            y=comp.maxionmos_y, bottom_height=comp.maxionmos_w,
            bottom=active, bottom_implant=ionimplant,
        )
        obuf_vss_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vss, wire=active, implant=ionimplant,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxionmos_y, height=comp.maxionmos_w,
        )
        layouter.add_wire(
            net=nets.vss, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": obuf_vss_m1bounds.left,
                "bottom": obuf_vss_m1bounds.top,
                "right": obuf_vss_m1bounds.right,
                "top": lib.iorow_height,
            }),
        )
        l = layouter.add_wire(
            net=nets.iovdd, well_net=nets.iovdd, wire=contact, x=x,
            y=comp.maxiopmos_y, bottom_height=comp.maxiopmos_w,
            bottom=active, bottom_implant=iopimplant, bottom_well=nwell,
        )
        obuf_iovdd_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.iovdd, well_net=nets.iovdd, wire=active, implant=iopimplant, well=nwell,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxiopmos_y, height=comp.maxiopmos_w,
        )
        layouter.add_wire(
            net=nets.iovdd, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": obuf_iovdd_m1bounds.left,
                "bottom": 0.0,
                "right": obuf_iovdd_m1bounds.right,
                "top": obuf_iovdd_m1bounds.bottom,
            }),
        )

        # Output buffer ionmos+iopmos
        x += max(
            comp.minionmos_contactgatepitch,
            comp.miniopmos_contactgatepitch,
        )
        l = layouter.place(insts.n_lvld_n_inv, x=x, y=comp.maxionmos_y)
        n_lvld_n_obuf_polybounds = l.bounds(mask=poly.mask)
        l = layouter.place(insts.p_lvld_n_inv, x=x, y=comp.maxiopmos_y)
        p_lvld_n_obuf_polybounds = l.bounds(mask=poly.mask)
        y = 0.5*(
            comp.maxiopmos_activetop + comp.maxionmos_activebottom
        )
        right = max((
            n_lvld_n_obuf_polybounds.right,
            p_lvld_n_obuf_polybounds.right,
        ))
        left = right - comp.minwidth4ext_polywithcontact
        l = layouter.add_wire(
            net=nets.lvld_n, wire=contact, bottom=poly,
            y=y, bottom_height=comp.minwidth_polywithcontact,
            **lib.layoutfab.spec4bound(via=contact, bound_spec={
                "bottom_left": left,
                "bottom_right": right,
            })
        )
        pad_lvld_n_obuf_polybounds = l.bounds(mask=poly.mask)
        pad_lvld_n_obuf_m1bounds = l.bounds(mask=metal1.mask)
        if pad_lvld_n_obuf_polybounds.top < n_lvld_n_obuf_polybounds.bottom:
            layouter.add_wire(
                net=nets.lvld_n, wire=poly,
                **lib.layoutfab.spec4bound(bound_spec={
                    "left": n_lvld_n_obuf_polybounds.left,
                    "bottom": pad_lvld_n_obuf_polybounds.top,
                    "right": n_lvld_n_obuf_polybounds.right,
                    "top": n_lvld_n_obuf_polybounds.bottom,
                }),
            )
        if p_lvld_n_obuf_polybounds.top < pad_lvld_n_obuf_polybounds.bottom:
            layouter.add_wire(
                net=nets.lvld_n, wire=poly,
                **lib.layoutfab.spec4bound(bound_spec={
                    "left": p_lvld_n_obuf_polybounds.left,
                    "bottom": p_lvld_n_obuf_polybounds.top,
                    "right": p_lvld_n_obuf_polybounds.right,
                    "top": pad_lvld_n_obuf_polybounds.bottom,
                }),
            )
        layouter.add_wire(
            net=nets.lvld_n, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": nch_lvld_n_m1bounds.right,
                "bottom": pad_lvld_n_obuf_m1bounds.bottom,
                "right": pad_lvld_n_obuf_m1bounds.left,
                "top": pad_lvld_n_obuf_m1bounds.top,
            }),
        )

        # Place right source-drain contacts
        x += max(
            comp.minionmos_contactgatepitch,
            comp.miniopmos_contactgatepitch,
        )
        l = layouter.add_wire(
            net=nets.o, wire=contact, x=x,
            y=comp.maxionmos_y, bottom_height=comp.maxionmos_w,
            bottom=active, bottom_implant=ionimplant,
        )
        nch_o_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.o, wire=active, implant=ionimplant,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxionmos_y, height=comp.maxionmos_w,
        )
        l = layouter.add_wire(
            net=nets.o, well_net=nets.iovdd, wire=contact, x=x,
            y=comp.maxiopmos_y, bottom_height=comp.maxiopmos_w,
            bottom=active, bottom_implant=iopimplant, bottom_well=nwell,
        )
        pch_o_m1bounds = l.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.o, well_net=nets.iovdd, wire=active, implant=iopimplant, well=nwell,
            x=x, width=comp.minwidth_activewithcontact,
            y=comp.maxiopmos_y, height=comp.maxiopmos_w,
        )
        layouter.add_wire(
            net=nets.o, wire=metal1,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": nch_o_m1bounds.left,
                "bottom": pch_o_m1bounds.top,
                "right": nch_o_m1bounds.right,
                "top": nch_o_m1bounds.bottom,
            })
        )
        left = nch_o_m1bounds.left
        right = left + comp.metal[2].minwidth_down
        bottom = pch_o_m1bounds.bottom
        top = nch_o_m1bounds.top
        l = layouter.add_wire(
            net=nets.o, wire=via1,
            **lib.layoutfab.spec4bound(via=via1, bound_spec={
                "top_left": left, "top_bottom": bottom,
                "top_right": right, "top_top": top,
            })
        )
        m2_o_m2bounds = l.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=nets.o, wire=metal2, **pin_args,
            **lib.layoutfab.spec4bound(bound_spec=m2_o_m2bounds),
        )

        #
        # Set boundary
        #

        cells_right = layout.bounds(mask=active.mask).right + 0.5*active.min_space
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=cells_right, top=lib.cells_height,
        )

        #
        # Well/bulk contacts
        #

        layouter.add_wire(
            net=nets.iovdd, wire=contact, well_net=nets.iovdd,
            bottom=active, bottom_implant=ionimplant, bottom_well=nwell,
            top_enclosure=comp.chm1_enclosure.wide(),
            x=0.5*cells_right, bottom_width=(cells_right - contact.min_space),
            y=0, bottom_height=comp.minwidth_activewithcontact,
        )
        l = layouter.add_wire(
            net=nets.iovdd, wire=active, implant=ionimplant,
            well_net=nets.iovdd, well=nwell,
            x=0.5*cells_right, width=cells_right,
            y=0, height=comp.minwidth_activewithcontact,
        )
        strap_iovdd_nwellbounds = l.bounds(mask=nwell.mask)
        layouter.add_wire(
            net=nets.iovdd, wire=nwell,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": strap_iovdd_nwellbounds.left,
                "bottom": strap_iovdd_nwellbounds.bottom,
                "right": strap_iovdd_nwellbounds.right,
                "top": lib.iorow_nwell_height,
            }),
        )
        layouter.add_wire(
            net=nets.iovdd, wire=metal1, pin=metal1pin,
            x=0.5*cells_right, width=cells_right,
            y=0, height=comp.metal[1].minwidth_updown,
        )

        layouter.add_wire(
            net=nets.vss, wire=contact, bottom=active,
            bottom_implant=pimplant, top_enclosure=comp.chm1_enclosure.wide(),
            x=0.5*cells_right, bottom_width=(cells_right - contact.min_space),
            y=lib.iorow_height, bottom_height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=nets.vss, wire=active, implant=pimplant,
            x=0.5*cells_right, width=cells_right,
            y=lib.iorow_height, height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=nets.vss, wire=metal1, pin=metal1pin,
            x=0.5*cells_right, width=cells_right,
            y=lib.iorow_height, height=comp.metal[1].minwidth_updown,
        )

        l = layouter.add_wire(
            net=nets.vdd, well_net=nets.vdd, wire=contact, bottom=active,
            bottom_implant=nimplant, bottom_well=nwell,
            top_enclosure=comp.chm1_enclosure.wide(),
            x=0.5*cells_right, bottom_width=(cells_right - contact.min_space),
            y=lib.cells_height, bottom_height=comp.minwidth_activewithcontact,
        )
        l = layouter.add_wire(
            net=nets.vdd, well_net=nets.vdd, wire=active,
            implant=nimplant, well=nwell,
            x=0.5*cells_right, width=cells_right,
            y=lib.cells_height, height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=nets.vdd, wire=metal1, pin=metal1pin,
            x=0.5*cells_right, width=cells_right,
            y=lib.cells_height, height=comp.metal[1].minwidth_updown,
        )
        strap_vdd_nwellbounds = l.bounds(mask=nwell.mask)
        layouter.add_wire(
            net=nets.vdd, wire=nwell,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": strap_vdd_nwellbounds.left,
                "bottom": lib.cells_height - lib.corerow_nwell_height,
                "right": strap_vdd_nwellbounds.right,
                "top": strap_vdd_nwellbounds.top,
            }),
        )

        # Thick oxide
        layouter.add_shape(prim=comp.ionmos.gate.oxide, net=None, shape=geo.Rect(
            left=-actox_enc, bottom=comp.io_oxidebottom,
            right=(cells_right + actox_enc), top=comp.io_oxidetop,
        ))


class _LevelDown(lbry._Cell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "LevelDown")

        self._add_circuit()
        self._add_layout()

    def _add_circuit(self):
        lib = self.lib
        comp = lib.computed

        circuit = self.new_circuit()

        # inverter with 5V gates
        n_hvinv = circuit.new_instance(
            "n_hvinv", lib.ionmos, w=comp.maxionmoscore_w,
        )
        p_hvinv = circuit.new_instance(
            "p_hvinv", lib.iopmos, w=comp.maxiopmoscore_w,
        )

        # second inverter, keep same w
        n_lvinv = circuit.new_instance(
            "n_lvinv", lib.nmos, w=comp.maxionmoscore_w,
        )
        p_lvinv = circuit.new_instance(
            "p_lvinv", lib.pmos, w=comp.maxiopmoscore_w,
        )

        prot = circuit.new_instance("secondprot", lib.get_cell("SecondaryProtection"))

        # Create the nets
        circuit.new_net("vdd", external=True, childports=(
            p_hvinv.ports.sourcedrain2, p_hvinv.ports.bulk,
            p_lvinv.ports.sourcedrain1, p_lvinv.ports.bulk,
        ))
        circuit.new_net("vss", external=True, childports=(
            n_hvinv.ports.sourcedrain2, n_hvinv.ports.bulk,
            n_lvinv.ports.sourcedrain1, n_lvinv.ports.bulk,
        ))
        circuit.new_net("iovdd", external=True, childports=(
            prot.ports.iovdd,
        ))
        circuit.new_net("iovss", external=True, childports=(
            prot.ports.iovss,
        ))
        circuit.new_net("pad", external=True, childports=(
            prot.ports.pad,
        ))
        circuit.new_net("padres", external=False, childports=(
            prot.ports.core,
            n_hvinv.ports.gate, p_hvinv.ports.gate,
        ))
        circuit.new_net("padres_n", external=False, childports=(
            n_hvinv.ports.sourcedrain1, p_hvinv.ports.sourcedrain1,
            n_lvinv.ports.gate, p_lvinv.ports.gate,
        ))
        circuit.new_net("core", external=True, childports=(
            n_lvinv.ports.sourcedrain2, p_lvinv.ports.sourcedrain2,
        ))

    def _add_layout(self):
        lib = self.lib
        comp = lib.computed

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        active = comp.active
        nimplant = comp.nimplant
        ionimplant = comp.ionimplant
        pimplant = comp.pimplant
        oxide = comp.oxide
        nwell = comp.nwell
        poly = comp.poly
        metal1 = comp.metal[1].prim
        metal1pin = metal1.pin[0]
        metal2 = comp.metal[2].prim
        metal2pin = metal2.pin[0]

        contact = comp.contact
        chm1_enclosure = contact.min_top_enclosure[0]
        via1 = comp.vias[1]

        hv_l = max(lib.ionmos.computed.min_l, lib.iopmos.computed.min_l)
        lv_l = max(lib.nmos.computed.min_l, lib.pmos.computed.min_l)
        hv_chgate_space = max(
            lib.ionmos.computed.min_contactgate_space,
            lib.iopmos.computed.min_contactgate_space,
        )
        lv_chgate_space = max(
            lib.nmos.computed.min_contactgate_space,
            lib.pmos.computed.min_contactgate_space,
        )

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout

        left = 0.5*comp.active.min_space + comp.activeoxide_enclosure.first
        x_left = left + 0.5*comp.minwidth_activewithcontact
        x_hv = x_left + 0.5*contact.width + hv_chgate_space + 0.5*hv_l
        x_mid = x_hv + 0.5*hv_l + max(
            hv_chgate_space + 0.5*contact.width,
            comp.iogateoxide_enclosure.first,
        )
        # FIXME: use oxide to gate spacing below, not iogate enclosure rule
        x_lv = x_mid + max(
            0.5*contact.width + lv_chgate_space,
            comp.iogateoxide_enclosure.first,
        ) + 0.5*lv_l
        x_right = x_lv + 0.5*lv_l + lv_chgate_space + 0.5*contact.width

        # HV inverter transistors
        layouter.place(insts.n_hvinv, x=x_hv, y=comp.maxionmoscore_y)
        layouter.place(insts.p_hvinv, x=x_hv, y=comp.maxiopmoscore_y)

        # LV inverter transistors
        layouter.place(insts.n_lvinv, x=x_lv, y=comp.maxionmoscore_y)
        layouter.place(insts.p_lvinv, x=x_lv, y=comp.maxiopmoscore_y)

        # FIXME: Allow contact to be drawn in between the transistors
        # Left source-drain contacts
        l_ch = layouter.add_wire(
            net=nets.padres_n, wire=contact, bottom=active, bottom_implant=nimplant,
            x=x_left, bottom_width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, bottom_height=comp.maxionmoscore_w,
        )
        m1_bounds1 = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.padres_n, wire=active, implant=nimplant, oxide=oxide,
            x=x_left, width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, height=comp.maxionmoscore_w,
        )
        l_ch = layouter.add_wire(
            net=nets.padres_n, wire=contact, bottom=active, bottom_implant=pimplant,
            bottom_well=nwell, well_net=nets.vdd,
            x=x_left, bottom_width=comp.minwidth_activewithcontact,
            y=comp.maxiopmoscore_y, bottom_height=comp.maxiopmoscore_w,
        )
        m1_bounds2 = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.padres_n, wire=active, implant=pimplant, oxide=oxide,
            well=nwell, well_net=nets.vdd,
            x=x_left, width=comp.minwidth_activewithcontact,
            y=comp.maxiopmoscore_y, height=comp.maxiopmoscore_w,
        )
        bottom = min(m1_bounds1.bottom, m1_bounds2.bottom)
        top = max(m1_bounds1.top, m1_bounds2.top)
        l_m1 = layouter.add_wire(
            net=nets.padres_n, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1_bounds1.left, "bottom": bottom,
                "right": m1_bounds1.right, "top": top,
            }),
        )
        m1left_bounds = l_m1.bounds()

        # Right source-drain contacts
        l_ch = layouter.add_wire(
            net=nets.core, wire=contact, bottom=active, bottom_implant=nimplant,
            x=x_right, bottom_width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, bottom_height=comp.maxionmoscore_w,
        )
        m1_bounds1 = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.core, wire=active, implant=nimplant,
            x=x_right, width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, height=comp.maxionmoscore_w,
        )
        l_ch = layouter.add_wire(
            net=nets.core, wire=contact, bottom=active, bottom_implant=pimplant,
            bottom_well=nwell, well_net=nets.vdd,
            x=x_right, bottom_width=comp.minwidth_activewithcontact,
            y=comp.maxiopmoscore_y, bottom_height=comp.maxiopmoscore_w,
        )
        m1_bounds2 = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.core, wire=active, implant=pimplant,
            well=nwell, well_net=nets.vdd,
            x=x_right, width=comp.minwidth_activewithcontact,
            y=comp.maxiopmoscore_y, height=comp.maxiopmoscore_w,
        )
        bottom = min(m1_bounds1.bottom, m1_bounds2.bottom)
        top = max(m1_bounds1.top, m1_bounds2.top)
        l_m1 = layouter.add_wire(
            net=nets.core, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1_bounds1.left, "bottom": bottom,
                "right": m1_bounds1.right, "top": top,
            }),
        )
        m1right_bounds = l_m1.bounds()
        m2_left = m1right_bounds.left
        m2_right = m1right_bounds.left + comp.metal[2].minwidth_down
        l_via = layouter.add_wire(
            net=nets.core, wire=via1, **fab.spec4bound(via=via1, bound_spec={
                "top_left": m2_left,
                "bottom_bottom": m1right_bounds.bottom,
                "top_right": m2_right,
                "bottom_top": m1right_bounds.top,
            }),
        )
        m2_bounds = l_via.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=nets.core, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec=m2_bounds),
        )

        # Middle source-drain contacts and transistor poly pads
        l_ch = layouter.add_wire(
            net=nets.vss, wire=contact, bottom=active, bottom_implant=nimplant,
            top_enclosure=chm1_enclosure.wide(),
            x=x_mid, bottom_width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, bottom_height=comp.maxionmoscore_w,
        )
        m1_bounds = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vss, wire=active, implant=nimplant,
            x=x_mid, width=comp.minwidth_activewithcontact,
            y=comp.maxionmoscore_y, height=comp.maxionmoscore_w,
        )
        l_m1 = layouter.add_wire(
            net=nets.vss, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1_bounds.left, "bottom": lib.iorow_height,
                "right": m1_bounds.right, "top": m1_bounds.top,
            }),
        )
        m1vss_bounds = l_m1.bounds()

        m1_right = m1right_bounds.left - metal1.min_space
        y_pad = lib.iorow_height + lib.corerow_pwell_height
        x_pad = m1_right - 0.5*comp.metal[1].minwidth_down
        l_pad = layouter.add_wire(
            net=nets.padres_n, wire=contact, bottom=poly, x=x_pad, y=y_pad,
            top_enclosure=chm1_enclosure.tall(),
        )
        m1polyconpadn_bounds = l_pad.bounds(mask=metal1.mask)
        m1_right = m1polyconpadn_bounds.left - metal1.min_space
        m1_bottom = m1vss_bounds.top + metal1.min_space
        m1_left = m1left_bounds.right + metal1.min_space
        x_pad = m1_left + 0.5*comp.metal[1].minwidth4ext_down
        l_pad = layouter.add_wire(
            net=nets.padres, wire=contact, bottom=poly, x=x_pad, y=y_pad,
            top_enclosure=chm1_enclosure.wide(),
        )
        m1polyconpad_bounds = l_pad.bounds(mask=metal1.mask)
        m1_top = m1polyconpad_bounds.top
        m1_width = m1_right - m1_left
        m1_height = m1_top - m1_bottom
        m1_area = m1_width*m1_height
        if metal1.min_area is not None:
            if m1_area < metal1.min_area:
                m1_height = lib.tech.on_grid(metal1.min_area, rounding="ceiling")
                m1_top = m1_bottom + m1_height
        layouter.add_wire(
            net=nets.padres, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1_left, "bottom": m1_bottom,
                "right": m1_right, "top": m1_top,
            }),
        )
        l_via = layouter.add_wire(
            net=nets.padres, wire=via1, **fab.spec4bound(via=via1, bound_spec={
                "bottom_left": m1_bounds.left,
                "bottom_bottom": m1_bounds.bottom,
                "bottom_right": m1_bounds.right,
                "bottom_top": m1_bounds.top,
            }),
        )
        m2padres_bounds1 = l_via.bounds(mask=metal2.mask)

        m1_bottom = m1_bounds.top + metal1.min_space
        m1_top = m1_bottom + metal1.min_width
        layouter.add_wire(
            net=nets.padres_n, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1left_bounds.left, "bottom": m1_bottom,
                "right": m1polyconpadn_bounds.right, "top": m1_top,
            }),
        )
        layouter.add_wire(
            net=nets.padres_n, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1polyconpadn_bounds.left,
                "bottom": m1polyconpadn_bounds.bottom,
                "right": m1polyconpadn_bounds.right,
                "top": m1_top,
            }),
        )

        m1_bottom = m1_top + metal1.min_space
        l_ch = layouter.add_wire(
            net=nets.vdd, wire=contact, bottom=active, bottom_implant=pimplant,
            bottom_well=nwell, well_net=nets.vdd,
            x=x_mid, bottom_width=comp.minwidth_activewithcontact,
            **fab.spec4bound(via=contact, bound_spec={
                "bottom_bottom": comp.maxiopmoscore_activebottom,
                "bottom_top": comp.maxiopmoscore_activetop,
                "top_enclosure": chm1_enclosure.wide(),
                "top_bottom": m1_bottom,
            }),
        )
        m1_bounds = l_ch.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=nets.vdd, wire=active, implant=pimplant,
            well=nwell, well_net=nets.vdd,
            x=x_mid, width=comp.minwidth_activewithcontact,
            y=comp.maxiopmoscore_y, height=comp.maxiopmoscore_w,
        )
        l_m1 = layouter.add_wire(
            net=nets.vdd, wire=metal1, **fab.spec4bound(bound_spec={
                "left": m1_bounds.left, "bottom": m1_bounds.bottom,
                "right": m1_bounds.right, "top": lib.cells_height,
            })
        )

        # Make some polygons in a rectangle shape
        for layer in (oxide, nimplant, pimplant):
            bounds = layout.bounds(mask=layer.mask)
            layout.add_primitive(prim=layer, **fab.spec4bound(bound_spec=bounds))

        # Set cell boundary
        cell_width = max(
            layout.bounds(mask=active.mask).right + 0.5*active.min_space,
            lib.get_cell("SecondaryProtection").layout.boundary.right,
        )
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=cell_width, top=lib.cells_height,
        )

        # Secondary protection
        _l_prot = layouter.inst_layout(insts.secondprot)
        _actvdd_bounds = _l_prot.bounds(net=nets.iovdd, mask=active.mask)
        l_prot = layouter.place(
            _l_prot, x=0,
            y=(-_actvdd_bounds.bottom - 0.5*comp.minwidth_activewithcontact),
        )

        net = nets.pad
        m2pin_bounds = l_prot.bounds(net=net, mask=metal2pin.mask)
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec=m2pin_bounds),
        )

        net = nets.padres
        m2padres_bounds2 = l_prot.bounds(net=net, mask=metal2pin.mask)
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2padres_bounds1.left, "bottom": m2padres_bounds2.bottom,
                "right": m2padres_bounds1.right, "top": m2padres_bounds1.top,
            }),
        )
        if m2padres_bounds1.right < m2padres_bounds2.left:
            layouter.add_wire(
                net=net, wire=metal2, **fab.spec4bound(bound_spec={
                    "left": m2padres_bounds1.left, "bottom": m2padres_bounds2.bottom,
                    "right": m2padres_bounds2.left, "top": m2padres_bounds2.top,
                }),
            )

        #
        # Well/bulk contacts
        #
        width = layout.boundary.right
        x_mid = 0.5*width

        layouter.add_wire(
            net=nets.iovdd, wire=metal1, pin=metal1pin,
            x=x_mid, width=width,
            y=0, height=comp.metal[1].minwidth_updown,
        )

        net = nets.vss
        layouter.add_wire(
            net=net, wire=contact, bottom=active,
            bottom_implant=pimplant, top_enclosure=comp.chm1_enclosure.wide(),
            x=x_mid, bottom_width=(width - contact.min_space),
            y=lib.iorow_height, bottom_height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=net, wire=active, implant=pimplant,
            x=x_mid, width=width,
            y=lib.iorow_height, height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            x=x_mid, width=width,
            y=lib.iorow_height, height=comp.metal[1].minwidth_updown,
        )

        l = layouter.add_wire(
            net=nets.vdd, well_net=nets.vdd, wire=contact, bottom=active,
            bottom_implant=nimplant, bottom_well=nwell,
            top_enclosure=comp.chm1_enclosure.wide(),
            x=x_mid, bottom_width=(width - contact.min_space),
            y=lib.cells_height, bottom_height=comp.minwidth_activewithcontact,
        )
        l = layouter.add_wire(
            net=nets.vdd, well_net=nets.vdd, wire=active,
            implant=nimplant, well=nwell,
            x=x_mid, width=width,
            y=lib.cells_height, height=comp.minwidth_activewithcontact,
        )
        layouter.add_wire(
            net=nets.vdd, wire=metal1, pin=metal1pin,
            x=x_mid, width=width,
            y=lib.cells_height, height=comp.metal[1].minwidth_updown,
        )
        strap_vdd_nwellbounds = l.bounds(mask=nwell.mask)
        layouter.add_wire(
            net=nets.vdd, wire=nwell,
            **lib.layoutfab.spec4bound(bound_spec={
                "left": strap_vdd_nwellbounds.left,
                "bottom": lib.cells_height - lib.corerow_nwell_height,
                "right": strap_vdd_nwellbounds.right,
                "top": strap_vdd_nwellbounds.top,
            }),
        )


class _CellsBulk(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "CellsBulk")

    def _create_circuit(self):
        ckt = self.new_circuit()

        ckt.new_net("vdd", external=True)
        ckt.new_net("vss", external=True)
        ckt.new_net("iovdd", external=True)
        ckt.new_net("iovss", external=True)

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame

        nets = self.circuit.nets

        active = comp.active
        nimplant = comp.nimplant
        pimplant = comp.pimplant
        nwell = comp.nwell
        contact = comp.contact
        metal1 = comp.metal[1].prim
        metal1pin = metal1.pin[0]
        via1 = comp.vias[1]
        metal2 = comp.metal[2].prim
        via2 = comp.vias[2]
        metal3 = comp.metal[3].prim

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout

        c_leveldown = lib.get_cell("LevelDown")
        ports_leveldown = c_leveldown.circuit.ports
        _l_leveldown = c_leveldown.layout
        ldm1iovddpin_bounds = _l_leveldown.bounds(mask=metal1pin.mask, net=ports_leveldown.iovdd)
        ldm1vsspin_bounds = _l_leveldown.bounds(mask=metal1pin.mask, net=ports_leveldown.vss)
        ldm1vddpin_bounds = _l_leveldown.bounds(mask=metal1pin.mask, net=ports_leveldown.vdd)
        ldact_bounds = _l_leveldown.bounds(mask=active.mask)
        ldm1_bounds = _l_leveldown.bounds(mask=metal1.mask)

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=3)
        _l_pclamp = c_pclamp.layout
        clact_bounds = _l_pclamp.bounds(mask=active.mask)
        clm1_bounds = _l_pclamp.bounds(mask=metal1.mask)
        clm3_bounds = _l_pclamp.bounds(mask=metal3.mask)

        net = nets.iovdd
        _l_ch = fab.new_primitivelayout(
            prim=contact, portnets={"conn": net},
            bottom=active, bottom_implant=nimplant, bottom_well=nwell,
            columns=8,
        )
        _act_bounds = _l_ch.bounds(mask=active.mask)
        y = ldm1iovddpin_bounds.center.y
        layouter.place(_l_ch, x=(-_act_bounds.left + contact.min_space), y=y)
        layouter.place(
            _l_ch,
            x=(lib.iocell_width - _act_bounds.right - contact.min_space),
            y=y,
        )
        layouter.add_wire(
            net=net, wire=active, implant=nimplant, well=nwell, well_net=net,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=_act_bounds.height,
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=ldm1iovddpin_bounds.height,
        )

        _l_via = fab.new_primitivelayout(
            prim=via1, portnets={"conn": net}, columns=2,
        )
        _m2_bounds = _l_via.bounds(mask=metal2.mask)
        l = layouter.place(
            _l_via,
            x=(lib.iocell_width - _m2_bounds.right - metal2.min_space),
            y=y,
        )
        m2_bounds1 = l.bounds(mask=metal2.mask)
        _l_via = fab.new_primitivelayout(
            prim=via2, portnets={"conn": net}, columns=2,
        )
        _m2_bounds = _l_via.bounds(mask=metal2.mask)
        l = layouter.place(
            _l_via,
            x=(lib.iocell_width - _m2_bounds.right - metal2.min_space),
            y=(frame.pclamp_y + clm3_bounds.top - _m2_bounds.top - frame.cells_y),
        )
        m2_bounds2 = l.bounds(mask=metal2.mask)
        layouter.add_wire(
            wire=metal2, net=net, **fab.spec4bound(bound_spec={
                "left": m2_bounds2.left, "bottom": m2_bounds2.bottom,
                "right": m2_bounds2.right, "top": m2_bounds1.top,
            }),
        )

        net = nets.vss
        _l_ch = fab.new_primitivelayout(
            prim=contact, portnets={"conn": net},
            bottom=active, bottom_implant=pimplant,
            columns=8,
        )
        _act_bounds = _l_ch.bounds(mask=active.mask)
        y = ldm1vsspin_bounds.center.y
        layouter.place(_l_ch, x=(-_act_bounds.left + contact.min_space), y=y)
        layouter.place(
            _l_ch,
            x=(lib.iocell_width - _act_bounds.right - contact.min_space),
            y=y,
        )
        layouter.add_wire(
            net=net, wire=active, implant=pimplant,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=_act_bounds.height,
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=ldm1vsspin_bounds.height,
        )
        _l_via = fab.new_primitivelayout(
            prim=via1, portnets={"conn": net}, columns=8,
        )
        _m2_bounds = _l_via.bounds(mask=metal2.mask)
        layouter.place(
            _l_via, x=(-_m2_bounds.left + metal2.min_space), y=y,
        )
        layouter.place(
            _l_via,
            x=(lib.iocell_width -_m2_bounds.right - metal2.min_space),
            y=y,
        )
        _l_via = fab.new_primitivelayout(
            prim=via2, portnets={"conn": net}, columns=8,
        )
        _m3_bounds = _l_via.bounds(mask=metal3.mask)
        layouter.place(
            _l_via, x=(-_m3_bounds.left + metal3.min_space), y=y,
        )
        layouter.place(
            _l_via,
            x=(lib.iocell_width -_m3_bounds.right - metal3.min_space),
            y=y,
        )

        net = nets.vdd
        _l_ch = fab.new_primitivelayout(
            prim=contact, portnets={"conn": net},
            bottom=active, bottom_implant=nimplant, bottom_well=nwell,
            columns=8,
        )
        _act_bounds = _l_ch.bounds(mask=active.mask)
        y = ldm1vddpin_bounds.center.y
        layouter.place(_l_ch, x=(-_act_bounds.left + contact.min_space), y=y)
        layouter.place(
            _l_ch,
            x=(lib.iocell_width - _act_bounds.right- contact.min_space),
            y=y,
        )
        layouter.add_wire(
            net=net, wire=active, implant=nimplant, well=nwell, well_net=net,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=_act_bounds.height,
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            x=0.5*lib.iocell_width, width=lib.iocell_width,
            y=y, height=ldm1vddpin_bounds.height,
        )
        _l_via = fab.new_primitivelayout(
            prim=via1, portnets={"conn": net}, columns=8,
        )
        _m2_bounds = _l_via.bounds(mask=metal2.mask)
        layouter.place(
            _l_via, x=(-_m2_bounds.left + metal2.min_space), y=y,
        )
        layouter.place(
            _l_via,
            x=(lib.iocell_width -_m2_bounds.right - metal2.min_space),
            y=y,
        )
        _l_via = fab.new_primitivelayout(
            prim=via2, portnets={"conn": net}, columns=8,
        )
        _m3_bounds = _l_via.bounds(mask=metal3.mask)
        layouter.place(
            _l_via, x=(-_m3_bounds.left + metal3.min_space), y=y,
        )
        layouter.place(
            _l_via,
            x=(lib.iocell_width -_m3_bounds.right - metal3.min_space),
            y=y,
        )

        net = nets.iovss
        left = 0.0
        right = lib.iocell_width
        act_bottom = frame.pclamp_y + clact_bounds.top - frame.cells_y
        m1_bottom = frame.pclamp_y + clm1_bounds.top - frame.cells_y
        act_top = ldact_bounds.bottom
        m1_top = ldm1_bounds.bottom

        layouter.add_wire(
            net=net, wire=contact, bottom=active, bottom_implant=pimplant,
            **fab.spec4bound(via=contact, bound_spec={
                "bottom_left": left + contact.min_space,
                "bottom_right": right - contact.min_space,
                "bottom_bottom": act_bottom + contact.min_space,
                "bottom_top": act_top - contact.min_space,
                "space": 2*contact.min_space,
            })
        )
        layouter.add_wire(
            net=net, wire=active, implant=pimplant,
            **fab.spec4bound(bound_spec={
                "left": left, "bottom": act_bottom,
                "right": right, "top": act_top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            **fab.spec4bound(bound_spec={
                "left": left, "bottom": m1_bottom,
                "right": right, "top": m1_top,
            }),
        )
        y = lib.frame.secondiovss_y - lib.frame.cells_y
        layouter.add_wire(
            net=net, wire=via1, x=4, bottom_width=5.0, y=y, bottom_height=10.0,
        )
        layouter.add_wire(
            net=net, wire=via1, x=(lib.iocell_width - 4.5), bottom_width=5.0, y=y, bottom_height=10.0,
        )
        layouter.add_wire(
            net=net, wire=via2, x=4, bottom_width=5.0, y=y, bottom_height=10.0,
        )
        layouter.add_wire(
            net=net, wire=via2, x=(lib.iocell_width - 4.5), bottom_width=5.0, y=y, bottom_height=10.0,
        )

        layout.boundary = layout.bounds()


class _GateDecode(lbry._Cell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "GateDecode")

        self.add_circuit()
        self.add_layout()

    def add_circuit(self):
        lib = self.lib
        stdcells = lib.stdcelllib.cells

        inv = stdcells.inv_x1
        nand = stdcells.na2_x1
        nor = stdcells.no2_x1
        levelup = lib.get_cell("LevelUp")

        ckt = self.new_circuit()

        oeinv = ckt.new_instance("oe_inv", inv)
        ngatenor = ckt.new_instance("ngate_nor", nor)
        ngatelu = ckt.new_instance("ngate_levelup", levelup)
        pgatenand = ckt.new_instance("pgate_nand", nand)
        pgatelu = ckt.new_instance("pgate_levelup", levelup)

        ckt.new_net("vdd", external=True, childports=(
            oeinv.ports.vdd, ngatenor.ports.vdd, ngatelu.ports.vdd,
            pgatenand.ports.vdd, pgatelu.ports.vdd,
        ))
        ckt.new_net("vss", external=True, childports=(
            oeinv.ports.vss, ngatenor.ports.vss, ngatelu.ports.vss,
            pgatenand.ports.vss, pgatelu.ports.vss,
        ))
        ckt.new_net("iovdd", external=True, childports=(
            ngatelu.ports.iovdd, pgatelu.ports.iovdd,
        ))

        ckt.new_net("d", external=True, childports=(
            ngatenor.ports.i0, pgatenand.ports.i0,
        ))
        ckt.new_net("de", external=True, childports=(
            oeinv.ports.i, pgatenand.ports.i1,
        ))
        ckt.new_net("de_n", external=False, childports=(
            oeinv.ports.nq, ngatenor.ports.i1,
        ))
        ckt.new_net("ngate_core", external=False, childports=(
            ngatenor.ports.nq, ngatelu.ports.i,
        ))
        ckt.new_net("ngate", external=True, childports=(ngatelu.ports.o))
        ckt.new_net("pgate_core", external=False, childports=(
            pgatenand.ports.nq, pgatelu.ports.i,
        ))
        ckt.new_net("pgate", external=True, childports=(pgatelu.ports.o))

    def add_layout(self):
        lib = self.lib
        stdcells = lib.stdcelllib.cells
        comp = lib.computed

        insts = self.circuit.instances
        nets = self.circuit.nets

        tie = stdcells.tie_x0
        levelup = lib.get_cell("LevelUp")
        via1 = comp.vias[1]
        metal1 = comp.metal[1].prim
        metal1pin = metal1.pin[0]
        metal2 = comp.metal[2].prim
        metal2pin = metal2.pin[0]
        metal2_pitch = comp.metal[2].minwidth4ext_updown + 2*metal2.min_space

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout

        # Place the cells
        l_ngatenor = layouter.place(insts.ngate_nor, x=0.0, y=0.0)
        l_pgatenand = layouter.place(
            insts.pgate_nand, x=cast(geo._Rectangular, l_ngatenor.boundary).right, y=0.0,
        )
        l_oeinv = layouter.place(
            insts.oe_inv, x=cast(geo._Rectangular, l_pgatenand.boundary).right, y=0.0,
        )

        y_lu = -levelup.layout.boundary.top - lib.levelup_core_space
        l_ngatelu = layouter.place(
            insts.ngate_levelup,
            x=tie.layout.boundary.right, y=y_lu,
        )
        l_pgatelu = layouter.place(
            insts.pgate_levelup, x=cast(geo._Rectangular, l_ngatelu.boundary).right, y=y_lu
        )

        # Set the boundary
        cell_left = 0.0
        cell_bottom = cast(geo._Rectangular, l_pgatelu.boundary).bottom
        cell_right = cast(geo._Rectangular, l_oeinv.boundary).right
        cell_top = cast(geo._Rectangular, l_oeinv.boundary).top
        layout.boundary = geo.Rect(
            left=cell_left, bottom=cell_bottom, right=cell_right, top=cell_top,
        )

        # Connect the nets
        net = nets.de
        m1pinbounds = tuple(p.bounds for p in chain(
            l_oeinv.filter_polygons(net=net, mask=metal1pin.mask, split=True),
            l_pgatenand.filter_polygons(net=net, mask=metal1pin.mask, split=True),
        ))
        xs = tuple(0.5*(bounds.left + bounds.right) for bounds in m1pinbounds)
        top = max(bounds.top for bounds in m1pinbounds)
        y = top - 0.5*comp.metal[2].minwidth4ext_updown
        l_vias = tuple(layouter.add_wire(net=net, wire=via1, x=x, y=y) for x in xs)
        viam2_boundss = tuple(l_via.bounds(mask=metal2.mask) for l_via in l_vias)
        viam2_bounds = geo.Rect(
            left=min(b.left for b in viam2_boundss),
            bottom=min(b.bottom for b in viam2_boundss),
            right=max(b.right for b in viam2_boundss),
            top=max(b.top for b in viam2_boundss),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec=viam2_bounds)
        )
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2.pin[0],
            **fab.spec4bound(bound_spec={
                "left": viam2_bounds.left,
                "bottom": viam2_bounds.bottom,
                "right": viam2_bounds.left + comp.metal[2].minwidth4ext_updown,
                "top": cast(geo._Rectangular, l_oeinv.boundary).top,
            }),
        )

        net = nets.d
        y -= metal2_pitch
        m1pinbounds = tuple(p.bounds for p in chain(
            l_ngatenor.filter_polygons(net=net, mask=metal1pin.mask, split=True),
            l_pgatenand.filter_polygons(net=net, mask=metal1pin.mask, split=True),
        ))
        xs = tuple(0.5*(bounds.left + bounds.right) for bounds in m1pinbounds)
        l_vias = tuple(layouter.add_wire(net=net, wire=via1, x=x, y=y) for x in xs)
        viam2_boundss = tuple(l_via.bounds(mask=metal2.mask) for l_via in l_vias)
        viam2_bounds = geo.Rect(
            left=min(b.left for b in viam2_boundss),
            bottom=min(b.bottom for b in viam2_boundss),
            right=max(b.right for b in viam2_boundss),
            top=max(b.top for b in viam2_boundss),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec=viam2_bounds)
        )
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2.pin[0],
            **fab.spec4bound(bound_spec={
                "left": viam2_bounds.left,
                "bottom": viam2_bounds.bottom,
                "right": viam2_bounds.left + comp.metal[2].minwidth4ext_updown,
                "top": cast(geo._Rectangular, l_oeinv.boundary).top,
            }),
        )

        net = nets.de_n
        y -= metal2_pitch
        m1pinbounds = tuple(p.bounds for p in chain(
            l_oeinv.filter_polygons(net=net, mask=metal1pin.mask, split=True),
            l_ngatenor.filter_polygons(net=net, mask=metal1pin.mask, split=True),
        ))
        xs = tuple(0.5*(bounds.left + bounds.right) for bounds in m1pinbounds)
        l_vias = tuple(layouter.add_wire(net=net, wire=via1, x=x, y=y) for x in xs)
        viam2_boundss = tuple(l_via.bounds(mask=metal2.mask) for l_via in l_vias)
        viam2_bounds = geo.Rect(
            left=min(b.left for b in viam2_boundss),
            bottom=min(b.bottom for b in viam2_boundss),
            right=max(b.right for b in viam2_boundss),
            top=max(b.top for b in viam2_boundss),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec=viam2_bounds)
        )

        net = nets.ngate_core
        m1bounds = l_ngatenor.bounds(mask=metal1pin.mask, net=net)
        x = m1bounds.right
        y = m1bounds.bottom + 0.5*comp.metal[2].minwidth4ext_updown
        l_via = layouter.add_wire(net=net, wire=via1, x=x, y=y)
        m2bounds1 = l_via.bounds(mask=metal2.mask)
        m2bounds2 = l_ngatelu.bounds(mask=metal2pin.mask, net=net)
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2bounds2.left, "bottom": m2bounds2.top,
                "right": m2bounds2.right, "top": m2bounds1.top,
            }),
        )
        if m2bounds2.left < m2bounds1.left:
            layouter.add_wire(
                net=net, wire=metal2, **fab.spec4bound(bound_spec={
                    "left": m2bounds2.left, "bottom": m2bounds1.bottom,
                    "right": m2bounds1.right, "top": m2bounds1.top,
                }),
            )
        else:
            layouter.add_wire(
                net=net, wire=metal2, **fab.spec4bound(bound_spec={
                    "left": m2bounds1.left, "bottom": m2bounds1.bottom,
                    "right": m2bounds2.right, "top": m2bounds1.top,
                }),
            )

        net = nets.pgate_core
        m1bounds = l_pgatenand.bounds(mask=metal1pin.mask, net=net)
        x = 0.5*(m1bounds.left + m1bounds.right)
        y = m1bounds.bottom + 0.5*comp.metal[2].minwidth4ext_updown
        l_via = layouter.add_wire(net=net, wire=via1, x=x, y=y)
        m2bounds1 = l_via.bounds(mask=metal2.mask)
        m2bounds2 = l_pgatelu.bounds(mask=metal2pin.mask, net=net)
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2bounds2.left, "bottom": m2bounds2.top,
                "right": m2bounds2.right, "top": m2bounds1.top,
            }),
        )
        if m2bounds2.left < m2bounds1.left:
            layouter.add_wire(
                net=net, wire=metal2, **fab.spec4bound(bound_spec={
                    "left": m2bounds2.left, "bottom": m2bounds1.bottom,
                    "right": m2bounds1.right, "top": m2bounds1.top,
                }),
            )
        else:
            layouter.add_wire(
                net=net, wire=metal2, **fab.spec4bound(bound_spec={
                    "left": m2bounds1.left, "bottom": m2bounds1.bottom,
                    "right": m2bounds2.right, "top": m2bounds1.top,
                }),
            )

        net = nets.ngate
        m2bounds = l_ngatelu.bounds(mask=metal2pin.mask, net=net)
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec=m2bounds),
        )

        net = nets.pgate
        m2bounds = l_pgatelu.bounds(mask=metal2pin.mask, net=net)
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec=m2bounds),
        )

        net = nets.vss
        lum1pin_bounds = l_pgatelu.bounds(mask=metal1pin.mask, net=net)
        invm1pin_bounds = l_oeinv.bounds(mask=metal1pin.mask, net=net)
        x = lum1pin_bounds.right - 0.5*comp.metal[1].minwidth4ext_up
        y = lum1pin_bounds.top
        l_via = layouter.add_wire(net=net, wire=via1, x=x, y=y)
        viam2_bounds1 = l_via.bounds(mask=metal2.mask)
        y = invm1pin_bounds.bottom + 0.5*comp.metal[1].minwidth4ext_up
        l_via = layouter.add_wire(net=net, wire=via1, x=x, y=y)
        viam2_bounds2 = l_via.bounds(mask=metal2.mask)
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": viam2_bounds1.left, "bottom": viam2_bounds1.bottom,
                "right": viam2_bounds1.right, "top": viam2_bounds2.top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            **fab.spec4bound(bound_spec={
                "left": cell_left, "bottom": invm1pin_bounds.bottom,
                "right": cell_right, "top": invm1pin_bounds.top,
            })
        )

        net = nets.vdd
        m1pin_bounds = l_oeinv.bounds(net=net, mask=metal1pin.mask)
        layout.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            **fab.spec4bound(bound_spec={
                "left": cell_left, "bottom": m1pin_bounds.bottom,
                "right": cell_right, "top": m1pin_bounds.top,
            })
        )
        x = cell_left + 0.5*comp.metal[1].minwidth4ext_up
        y = m1pin_bounds.bottom + 0.5*comp.metal[1].minwidth4ext_up
        l_via = layout.add_wire(net=net, wire=via1, x=x, y=y)
        viam2_bounds1 = l_via.bounds(mask=metal2.mask)
        m1pin_bounds = l_ngatelu.bounds(net=net, mask=metal1pin.mask)
        y = m1pin_bounds.top - 0.5*comp.metal[1].minwidth4ext_up
        l_via = layout.add_wire(net=net, wire=via1, x=x, y=y)
        viam2_bounds2 = l_via.bounds(mask=metal2.mask)
        viam1_bounds = l_via.bounds(mask=metal1.mask)
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": viam2_bounds1.left, "bottom": viam2_bounds2.bottom,
                "right": viam2_bounds1.right, "top": viam2_bounds1.top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal1, **fab.spec4bound(bound_spec={
                "left": viam1_bounds.left, "bottom": m1pin_bounds.bottom,
                "right": m1pin_bounds.left, "top": m1pin_bounds.top,
            }),
        )

        net = nets.iovdd
        m1pin_bounds = l_ngatelu.bounds(net=net, mask=metal1pin.mask)
        layouter.add_wire(
            net=net, wire=metal1, pin=metal1pin,
            **fab.spec4bound(bound_spec={
                "left": cell_left, "bottom": m1pin_bounds.bottom,
                "right": cell_right, "top": m1pin_bounds.top,
            }),
        )


class _PadOut(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadOut")

    def _create_circuit(self):
        lib=self.lib
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        circuit.new_net("d", external=True)
        circuit.new_net("de", external=True)
        pad = circuit.new_net("pad", external=True)

        ngate = circuit.new_net("ngate", external=False)
        pgate = circuit.new_net("pgate", external=False)

        c_pad = lib.pad(width=lib.pad_width, height=lib.pad_height)
        i_pad = circuit.new_instance("pad", c_pad)
        pad.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=3)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        pad.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss
        ngate.childports += i_nclamp.ports.gate

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=3)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        pad.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss
        pgate.childports += i_pclamp.ports.gate

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

        i_gatedec = circuit.new_instance("gatedec", lib.get_cell("GateDecode"))
        for name in ("vss", "vdd", "iovdd", "d", "de", "ngate", "pgate"):
            circuit.nets[name].childports += i_gatedec.ports[name]

    def _create_layout(self):
        lib = self.lib
        frame = lib.frame
        comp = lib.computed
        metal = comp.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal2pin = metal2.pin[0]
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.pad, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_nclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": bounds.top,
                        "right": bounds.right, "top": padm2_bounds.bottom,
                    })
                )
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": padm2_bounds.top,
                        "right": bounds.right, "top": bounds.bottom,
                    })
                )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)

        # Gate decoder + interconnect
        _l_gatedec = layouter.inst_layout(insts.gatedec)
        dm2pin_bounds = _l_gatedec.bounds(net=nets.d, mask=metal2pin.mask)
        dem2pin_bounds = _l_gatedec.bounds(net=nets.de, mask=metal2pin.mask)
        x = 0.5*lib.iocell_width - 0.5*(dm2pin_bounds.left + dem2pin_bounds.right)
        l_gatedec = layouter.place(
            _l_gatedec, x=x,
            y=(frame.cells_y - cast(geo._Rectangular, _l_gatedec.boundary).bottom),
        )

        # Bring pins to top
        for name in ("d", "de"):
            net = nets[name]
            m2pin_bounds = l_gatedec.bounds(net=net, mask=metal2pin.mask)
            layouter.add_wire(
                net=net, wire=metal2, pin=metal2pin,
                **fab.spec4bound(bound_spec=m2pin_bounds),
            )

        net = nets.pgate
        m2pin_bounds1 = l_gatedec.bounds(net=net, mask=metal2pin.mask)
        m2pin_bounds2 = l_pclamp.bounds(net=net, mask=metal2pin.mask)
        m2_width = comp.metal[2].minwidth4ext_updown
        bottom = cast(geo._Rectangular, l_pclamp.boundary).top + 2*metal2.min_space
        y = bottom + 0.5*m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds1.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": m2pin_bounds1.bottom,
            }),
        )
        top = bottom + m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds2.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds2.left, "bottom": m2pin_bounds2.top,
                "right": m2pin_bounds2.right, "top": top,
            }),
        )

        net = nets.ngate
        m2pin_bounds1 = l_gatedec.bounds(net=net, mask=metal2pin.mask)
        m2pin_bounds2 = l_nclamp.bounds(net=net, mask=metal2pin.mask)
        y += m2_width + 2*metal2.min_space
        bottom = y - 0.5*m2_width
        top = y + 0.5*m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds1.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": m2pin_bounds1.bottom,
            }),
        )
        left = 0.5*lib.metal_bigspace
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": top,
            }),
        )
        right = left + m2_width
        bottom = m2pin_bounds2.top - m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom, "right": right, "top": top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": m2pin_bounds2.left, "top": m2pin_bounds2.top,
            }),
        )


class _PadIn(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadIn")

    def _create_circuit(self):
        lib=self.lib
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        s = circuit.new_net("s", external=True)
        pad = circuit.new_net("pad", external=True)

        c_pad = lib.pad(width=lib.pad_width, height=lib.pad_height)
        i_pad = circuit.new_instance("pad", c_pad)
        pad.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        pad.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        pad.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

        i_leveldown = circuit.new_instance("leveldown", lib.get_cell("LevelDown"))
        for name in ("vss", "vdd", "iovss", "iovdd", "pad"):
            circuit.nets[name].childports += i_leveldown.ports[name]
        s.childports += i_leveldown.ports.core

    def _create_layout(self):
        lib = self.lib
        frame = lib.frame
        comp = lib.computed
        metal = comp.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal2pin = metal2.pin[0]
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.pad, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_nclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": bounds.top,
                        "right": bounds.right, "top": padm2_bounds.bottom,
                    })
                )
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": padm2_bounds.top,
                        "right": bounds.right, "top": bounds.bottom,
                    })
                )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)

        # LevelDown + interconnect
        _l_ld = layouter.inst_layout(insts.leveldown)
        _sm2pin_bounds = _l_ld.bounds(net=nets.s, mask=metal2pin.mask)
        x = 0.5*lib.iocell_width - 0.5*(_sm2pin_bounds.left + _sm2pin_bounds.right)
        l_ld = layouter.place(_l_ld, x=x, y=frame.cells_y)

        net = nets.s
        m2pin_bounds = l_ld.bounds(net=net, mask=metal2pin.mask)
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec={
                "left": m2pin_bounds.left, "bottom": m2pin_bounds.bottom,
                "right": m2pin_bounds.right, "top": lib.iocell_height,
            }),
        )

        net = nets.pad
        m2pin_bounds = l_ld.bounds(net=net, mask=metal2pin.mask)
        clamp_bounds = None
        for polygon in l_pclamp.filter_polygons(
            net=nets.pad, mask=metal2.mask, split=True,
        ):
            bounds = polygon.bounds
            if clamp_bounds is None:
                if bounds.left >= m2pin_bounds.left:
                    clamp_bounds = bounds
            else:
                if (
                    (bounds.left >= m2pin_bounds.left)
                    and (bounds.left < clamp_bounds.left)
                ):
                    clamp_bounds = bounds
        assert clamp_bounds is not None, "Internal error"
        m2_width = comp.metal[2].minwidth4ext_updown
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds.left, "bottom": m2pin_bounds.bottom - m2_width,
                "right": clamp_bounds.left + m2_width, "top": m2pin_bounds.bottom,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": clamp_bounds.left, "bottom": clamp_bounds.top,
                "right": clamp_bounds.left + m2_width, "top": m2pin_bounds.bottom,
            }),
        )


class _PadInOut(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadInOut")

    def _create_circuit(self):
        lib=self.lib
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        s = circuit.new_net("s", external=True)
        circuit.new_net("d", external=True)
        circuit.new_net("de", external=True)
        pad = circuit.new_net("pad", external=True)

        ngate = circuit.new_net("ngate", external=False)
        pgate = circuit.new_net("pgate", external=False)

        c_pad = lib.pad(width=lib.pad_width, height=lib.pad_height)
        i_pad = circuit.new_instance("pad", c_pad)
        pad.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=3)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        pad.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss
        ngate.childports += i_nclamp.ports.gate

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=3)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        pad.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss
        pgate.childports += i_pclamp.ports.gate

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

        i_gatedec = circuit.new_instance("gatedec", lib.get_cell("GateDecode"))
        for name in ("vss", "vdd", "iovdd", "d", "de", "ngate", "pgate"):
            circuit.nets[name].childports += i_gatedec.ports[name]

        i_leveldown = circuit.new_instance("leveldown", lib.get_cell("LevelDown"))
        for name in ("vss", "vdd", "iovss", "iovdd", "pad"):
            circuit.nets[name].childports += i_leveldown.ports[name]
        s.childports += i_leveldown.ports.core

    def _create_layout(self):
        lib = self.lib
        frame = lib.frame
        comp = lib.computed
        metal = comp.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal2pin = metal2.pin[0]
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.pad, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_nclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": bounds.top,
                        "right": bounds.right, "top": padm2_bounds.bottom,
                    })
                )
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": padm2_bounds.top,
                        "right": bounds.right, "top": bounds.bottom,
                    })
                )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)

        # Gate decoder + interconnect
        _l_gatedec = layouter.inst_layout(insts.gatedec)
        dm2pin_bounds = _l_gatedec.bounds(net=nets.d, mask=metal2pin.mask)
        dem2pin_bounds = _l_gatedec.bounds(net=nets.de, mask=metal2pin.mask)
        x = lib.tech.on_grid(
            0.25*lib.iocell_width - 0.5*(dm2pin_bounds.left + dem2pin_bounds.right),
        )
        l_gatedec = layouter.place(
            _l_gatedec, x=x,
            y=(frame.cells_y - cast(geo._Rectangular, _l_gatedec.boundary).bottom),
        )

        # Bring pins to top
        for name in ("d", "de"):
            net = nets[name]
            m2pin_bounds = l_gatedec.bounds(net=net, mask=metal2pin.mask)
            layouter.add_wire(
                net=net, wire=metal2, pin=metal2pin,
                **fab.spec4bound(bound_spec=m2pin_bounds),
            )

        net = nets.pgate
        m2pin_bounds1 = l_gatedec.bounds(net=net, mask=metal2pin.mask)
        m2pin_bounds2 = l_pclamp.bounds(net=net, mask=metal2pin.mask)
        m2_width = comp.metal[2].minwidth4ext_updown
        bottom = cast(geo._Rectangular, l_pclamp.boundary).top + 2*metal2.min_space
        y = bottom + 0.5*m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds1.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": m2pin_bounds1.bottom,
            }),
        )
        top = bottom + m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds2.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds2.left, "bottom": m2pin_bounds2.top,
                "right": m2pin_bounds2.right, "top": top,
            }),
        )

        net = nets.ngate
        m2pin_bounds1 = l_gatedec.bounds(net=net, mask=metal2pin.mask)
        m2pin_bounds2 = l_nclamp.bounds(net=net, mask=metal2pin.mask)
        y += m2_width + 2*metal2.min_space
        bottom = y - 0.5*m2_width
        top = y + 0.5*m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds1.left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": m2pin_bounds1.bottom,
            }),
        )
        left = 0.5*lib.metal_bigspace
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": m2pin_bounds1.right, "top": top,
            }),
        )
        right = left + m2_width
        bottom = m2pin_bounds2.top - m2_width
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom, "right": right, "top": top,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": m2pin_bounds2.left, "top": m2pin_bounds2.top,
            }),
        )

        # LevelDown + interconnect
        _l_ld = layouter.inst_layout(insts.leveldown)
        _sm2pin_bounds = _l_ld.bounds(net=nets.s, mask=metal2pin.mask)
        x = lib.tech.on_grid(
            0.75*lib.iocell_width - 0.5*(_sm2pin_bounds.left + _sm2pin_bounds.right),
        )
        l_ld = layouter.place(_l_ld, x=x, y=frame.cells_y)

        net = nets.s
        m2pin_bounds = l_ld.bounds(net=net, mask=metal2pin.mask)
        layouter.add_wire(
            net=net, wire=metal2, pin=metal2pin,
            **fab.spec4bound(bound_spec={
                "left": m2pin_bounds.left, "bottom": m2pin_bounds.bottom,
                "right": m2pin_bounds.right, "top": lib.iocell_height,
            }),
        )

        net = nets.pad
        m2pin_bounds = l_ld.bounds(net=net, mask=metal2pin.mask)
        clamp_bounds = None
        for polygon in l_pclamp.filter_polygons(
            net=nets.pad, mask=metal2.mask, split=True,
        ):
            bounds = polygon.bounds
            if clamp_bounds is None:
                if bounds.left >= m2pin_bounds.left:
                    clamp_bounds = bounds
            else:
                if (
                    (bounds.left >= m2pin_bounds.left)
                    and (bounds.left < clamp_bounds.left)
                ):
                    clamp_bounds = bounds
        assert clamp_bounds is not None, "Internal error"
        m2_width = comp.metal[2].minwidth4ext_updown
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": m2pin_bounds.left,
                "bottom": m2pin_bounds.bottom - m2_width,
                "right": clamp_bounds.left + m2_width,
                "top": m2pin_bounds.bottom,
            }),
        )
        layouter.add_wire(
            net=net, wire=metal2, **fab.spec4bound(bound_spec={
                "left": clamp_bounds.left, "bottom":clamp_bounds.top,
                "right": clamp_bounds.left + m2_width, "top": m2pin_bounds.bottom,
            }),
        )


class _PadAnalog(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadAnalog")

    def _create_circuit(self):
        lib = self.lib
        frame = lib.frame
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        pad = circuit.new_net("pad", external=True)
        padres = circuit.new_net("padres", external=True)

        i_pad = circuit.new_instance("pad", frame.pad)
        pad.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount_analog, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        pad.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount_analog, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        pad.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

        c_secondprot = lib.get_cell("SecondaryProtection")
        i_secondprot = circuit.new_instance("secondprot", c_secondprot)
        iovss.childports += i_secondprot.ports.iovss
        iovdd.childports += i_secondprot.ports.iovdd
        pad.childports += i_secondprot.ports.pad
        padres.childports += i_secondprot.ports.core

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame

        active = comp.active
        metal = comp.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]
        via2 = comp.vias[2]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.pad, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_nclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": bounds.top,
                        "right": bounds.right, "top": padm2_bounds.bottom,
                    })
                )
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.pad),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                layouter.add_wire(
                    wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": padm2_bounds.top,
                        "right": bounds.right, "top": bounds.bottom,
                    })
                )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)

        # Place the secondary protection
        pclampact_bounds = l_pclamp.bounds(mask=comp.active.mask)
        # Search for pad pin closest to the middle of the cell
        x_clamppin = None
        pinmask = metal2.pin[0].mask
        hw = 0.5*lib.iocell_width
        clamppinm2_bounds: Optional[geo.Rect] = None
        for polygon in filter(
            lambda p: p.mask == pinmask, l_pclamp.net_polygons(nets.pad),
        ):
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x_p = 0.5*(bounds.left + bounds.right)
                if (
                    (x_clamppin is None)
                    or (abs(x_p - hw) < abs(x_clamppin - hw))
                ):
                    x_clamppin = x_p
                    assert isinstance(bounds, geo.Rect)
                    clamppinm2_bounds = bounds
        assert x_clamppin is not None
        assert clamppinm2_bounds is not None
        _l_secondprot = layouter.inst_layout(insts.secondprot)
        _actvdd_bounds = _l_secondprot.bounds(net=nets.iovdd, mask=active.mask)
        y = frame.cells_y - _actvdd_bounds.bottom - 0.5*comp.minwidth_activewithcontact
        _protpadpin_bounds = _l_secondprot.bounds(mask=pinmask, net=nets.pad)
        x_protpadpin = 0.5*(_protpadpin_bounds.left + _protpadpin_bounds.right)
        # Center pins
        x = lib.tech.on_grid(x_clamppin - x_protpadpin)
        l_secondprot = layouter.place(_l_secondprot, x=x, y=y)
        secondprotm2_bounds = l_secondprot.bounds(mask=metal2.mask)
        protpadpin_bounds = l_secondprot.bounds(mask=pinmask, net=nets.pad)
        protpadrespin_bounds = l_secondprot.bounds(mask=pinmask, net=nets.padres)

        # Connect pins of secondary protection
        layouter.add_wire(
            wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                "left": protpadpin_bounds.left, "bottom": clamppinm2_bounds.top,
                "right": protpadpin_bounds.right, "top": protpadpin_bounds.top,
            })
        )

        right = secondprotm2_bounds.left - 2*metal2.min_space
        left = right - comp.metal[2].minwidth4ext_updown
        top2 = protpadpin_bounds.top
        bottom = top2 - comp.metal[2].minwidth4ext_updown
        layout.add_wire(
            wire=metal2, net=nets.pad, **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": protpadpin_bounds.right, "top": top2,
            }),
        )
        layout.add_wire(
            wire=metal2, pin=metal2.pin[0], net=nets.pad,
            **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom,
                "right": right, "top": lib.iocell_height,
            }),
        )

        right = protpadrespin_bounds.right
        left = right - comp.metal[2].minwidth4ext_updown
        bottom = protpadrespin_bounds.top
        top = lib.iocell_height
        layout.add_wire(
            wire=metal2, pin=metal2.pin[0], net=nets.padres,
            **fab.spec4bound(bound_spec={
                "left": left, "bottom": bottom, "right": right, "top": top,
            }),
        )


class _PadIOVss(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadIOVss")

    def _create_circuit(self):
        lib = self.lib
        frame = lib.frame
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        i_pad = circuit.new_instance("pad", frame.pad)
        iovss.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += (i_nclamp.ports.pad, i_nclamp.ports.iovss)

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += (i_pclamp.ports.pad, i_pclamp.ports.iovss)

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame
        metal = lib.computed.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        metal2pin = metal2.pin[0]
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]
        via2 = comp.vias[2]

        def pinpolygons(polygons: Iterable[Union[lay.MaskPolygon, geo.MaskShape]]):
            return tuple(filter(lambda p: p.mask in trackpinmasks, polygons))

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in l_nclamp.filter_polygons(
            net=nets.iovss, mask=metal2pin.mask, split=True,
        ):
            for metal in (metal2, metal3, metal4, metal5):
                bounds = polygon.bounds
                layouter.add_wire(
                    wire=metal, net=nets.iovss, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": bounds.bottom,
                        "right": bounds.right, "top": padm2_bounds.bottom,
                    })
                )
        m3track_bounds = l_secondiovss.bounds(mask=metal3.mask)
        for polygon in l_pclamp.filter_polygons(
            net=nets.iovss, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.iovss, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": padm2_bounds.top,
                    "right": bounds.right, "top": m3track_bounds.top,
                }),
            )
            layout.add_wire(
                wire=via2, net=nets.iovss,
                **fab.spec4bound(via=via2, bound_spec={
                    "space": (lib.ioringvia_pitch - via2.width),
                    "bottom_left": bounds.left,
                    "bottom_bottom": m3track_bounds.bottom,
                    "bottom_right": bounds.right,
                    "bottom_top": m3track_bounds.top,
                }),
            )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)


class _PadIOVdd(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadIOVdd")

    def _create_circuit(self):
        lib = self.lib
        frame = lib.frame
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        i_pad = circuit.new_instance("pad", frame.pad)
        iovdd.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        iovdd.childports += (i_nclamp.ports.pad, i_nclamp.ports.iovdd)
        iovss.childports += i_nclamp.ports.iovss

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        iovdd.childports += (i_pclamp.ports.pad, i_pclamp.ports.iovdd)
        iovss.childports += i_pclamp.ports.iovss

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame
        metal = lib.computed.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        metal2pin = metal2.pin[0]
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]
        via2 = comp.vias[2]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in l_nclamp.filter_polygons(
            net=nets.iovdd, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.iovdd, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": bounds.bottom,
                    "right": bounds.right, "top": padm2_bounds.bottom,
                })
            )
        for polygon in l_pclamp.filter_polygons(
            net=nets.iovdd, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            for metal in (metal2, metal3, metal4, metal5):
                layouter.add_wire(
                    wire=metal, net=nets.iovdd, **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": padm2_bounds.top,
                        "right": bounds.right, "top": bounds.top,
                    }),
                )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            # Iterate over bounds of individual shapes
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)


class _PadVss(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadVss")

    def _create_circuit(self):
        lib = self.lib
        frame = lib.frame
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        i_pad = circuit.new_instance("pad", frame.pad)
        vss.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        vss.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        vss.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame
        metal = lib.computed.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        metal2pin = metal2.pin[0]
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]
        via2 = comp.vias[2]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.vss, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in l_nclamp.filter_polygons(
            net=nets.vss, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.vss, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": bounds.bottom,
                    "right": bounds.right, "top": padm2_bounds.bottom,
                })
            )
        m3track_bounds = l_vddvss.bounds(mask=metal3.mask, net=nets.vss)
        for polygon in l_pclamp.filter_polygons(
            net=nets.vss, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.vss, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": padm2_bounds.top,
                    "right": bounds.right, "top": m3track_bounds.top,
                }),
            )
            layouter.add_wire(
                wire=via2, net=nets.vss,
                **fab.spec4bound(via=via2, bound_spec={
                    "space": lib.ioringvia_pitch - via2.width,
                    "bottom_left": bounds.left,
                    "bottom_bottom": m3track_bounds.bottom,
                    "bottom_right": bounds.right,
                    "bottom_top": m3track_bounds.top,
                })
            )

        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)


class _PadVdd(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "IOPadVdd")

    def _create_circuit(self):
        lib = self.lib
        frame = lib.frame
        circuit = self.new_circuit()

        vdd = circuit.new_net("vdd", external=True)
        vss = circuit.new_net("vss", external=True)
        iovdd = circuit.new_net("iovdd", external=True)
        iovss = circuit.new_net("iovss", external=True)

        i_pad = circuit.new_instance("pad", frame.pad)
        vdd.childports += i_pad.ports.pad

        c_nclamp = lib.clamp(type_="n", n_trans=lib.clampcount, n_drive=0)
        i_nclamp = circuit.new_instance("nclamp", c_nclamp)
        vdd.childports += i_nclamp.ports.pad
        iovdd.childports += i_nclamp.ports.iovdd
        iovss.childports += i_nclamp.ports.iovss

        c_pclamp = lib.clamp(type_="p", n_trans=lib.clampcount, n_drive=0)
        i_pclamp = circuit.new_instance("pclamp", c_pclamp)
        vdd.childports += i_pclamp.ports.pad
        iovdd.childports += i_pclamp.ports.iovdd
        iovss.childports += i_pclamp.ports.iovss

        c_cellsbulk = lib.get_cell("CellsBulk")
        i_cellsbulk = circuit.new_instance("cellsbulk", c_cellsbulk)
        vdd.childports += i_cellsbulk.ports.vdd
        vss.childports += i_cellsbulk.ports.vss
        iovdd.childports += i_cellsbulk.ports.iovdd
        iovss.childports += i_cellsbulk.ports.iovss

        c_secondiovss = lib.iotracksegment(height=10.0)
        i_secondiovss = circuit.new_instance("secondiovss", c_secondiovss)
        iovss.childports += i_secondiovss.ports.track

        c_vddvss = lib.ioduotracksegment(height=32.0, space=lib.ioring_space)
        i_vddvss = circuit.new_instance("vddvss", c_vddvss)
        vss.childports += i_vddvss.ports.track1
        vdd.childports += i_vddvss.ports.track2

    def _create_layout(self):
        lib = self.lib
        comp = lib.computed
        frame = lib.frame
        metal = lib.computed.metal
        metal1 = metal[1].prim
        metal2 = metal[2].prim
        metal3 = metal[3].prim
        metal4 = metal[4].prim
        metal5 = metal[5].prim
        metal6 = metal[6].prim
        metal2pin = metal2.pin[0]
        trackpinmasks = set(m.pin[0].mask for m in (
            metal3, metal4, metal5, metal6,
        ))
        via1 = comp.vias[1]
        via2 = comp.vias[2]

        def pinpolygons(polygons):
            return filter(lambda p: p.mask in trackpinmasks, polygons)

        circuit = self.circuit
        insts = circuit.instances
        nets = circuit.nets

        layouter = self.new_circuitlayouter()
        fab = layouter.fab
        layout = self.layout
        layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=lib.iocell_width, top=lib.iocell_height,
        )

        # PAD
        x = 0.5*lib.iocell_width
        l_pad = layouter.place(insts.pad, x=x, y=frame.pad_y)
        padm2_bounds = l_pad.bounds(mask=metal2.mask)
        for polygon in pinpolygons(l_pad.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.vdd, maskshape=polygon)

        # iovss & iovdd
        l_nclamp = layouter.place(insts.nclamp, x=0.0, y=frame.nclamp_y)
        for polygon in pinpolygons(l_nclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_pclamp = layouter.place(insts.pclamp, x=0.0, y=frame.pclamp_y)
        for polygon in pinpolygons(l_pclamp.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovdd, maskshape=polygon)
        l_secondiovss = layouter.place(
            insts.secondiovss, x=0.5*lib.iocell_width, y=frame.secondiovss_y)
        for polygon in pinpolygons(l_secondiovss.polygons):
            assert isinstance(polygon, geo.MaskShape)
            layout.add_maskshape(net=nets.iovss, maskshape=polygon)
        l_vddvss = layouter.place(
            insts.vddvss, x=0.5*lib.iocell_width, y=frame.vddvss_y,
        )
        for net in (nets.vss, nets.vdd):
            for polygon in pinpolygons(l_vddvss.net_polygons(net)):
                assert isinstance(polygon, geo.MaskShape)
                layout.add_maskshape(net=net, maskshape=polygon)

        # Connect clamps to pad
        for polygon in l_nclamp.filter_polygons(
            net=nets.vdd, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.vdd, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": bounds.bottom,
                    "right": bounds.right, "top": padm2_bounds.bottom,
                }),
            )
        m3track_bounds = l_vddvss.bounds(mask=metal3.mask, net=nets.vdd)
        for polygon in l_pclamp.filter_polygons(
            net=nets.vdd, mask=metal2pin.mask, split=True,
        ):
            bounds = polygon.bounds
            layouter.add_wire(
                wire=metal2, net=nets.vdd, **fab.spec4bound(bound_spec={
                    "left": bounds.left, "bottom": padm2_bounds.top,
                    "right": bounds.right, "top": m3track_bounds.top,
                }),
            )
            layouter.add_wire(
                wire=via2, net=nets.vdd,
                **fab.spec4bound(via=via2, bound_spec={
                    "space": lib.ioringvia_pitch - via2.width,
                    "bottom_left": bounds.left,
                    "bottom_bottom": m3track_bounds.bottom,
                    "bottom_right": bounds.right,
                    "bottom_top": m3track_bounds.top,
                })
            )
        # Draw guardring around pad and connect to iovdd track
        bottom = cast(geo._Rectangular, l_nclamp.boundary).top
        top = cast(geo._Rectangular, l_pclamp.boundary).bottom - comp.guardring_space
        _l_guardring = hlp.guardring(
            lib=lib, net=nets.iovss, type_="n",
            width=lib.iocell_width, height=(top - bottom),
        )
        l_guardring = layouter.place(
            _l_guardring, x=0.5*lib.iocell_width, y=0.5*(bottom + top),
        )
        guardringm1_bounds = l_guardring.bounds(mask=metal1.mask)
        viatop = guardringm1_bounds.top
        viabottom = viatop - comp.metal[2].minwidth4ext_updown
        for polygon in filter(
            lambda p: p.mask == metal2.mask,
            l_pclamp.net_polygons(net=nets.iovdd),
        ):
            for bounds in _iterate_polygonbounds(polygon=polygon):
                x = 0.5*(bounds.left + bounds.right)
                l_via = layouter.add_wire(
                    net=nets.iovdd, wire=via1, **fab.spec4bound(
                        via=via1, bound_spec={
                            "top_left": bounds.left,
                            "top_bottom": viabottom,
                            "top_right": bounds.right,
                            "top_top": viatop,
                        },
                    )
                )
                viam2_bounds = l_via.bounds(mask=metal2.mask)
                layouter.add_wire(
                    net=nets.iovdd, wire=metal2,
                    **fab.spec4bound(bound_spec={
                        "left": bounds.left, "bottom": viam2_bounds.bottom,
                        "right": bounds.right, "top": bounds.bottom,
                    }),
                )

        # Bulk/well connection cell
        l_cellsbulk = layouter.place(insts.cellsbulk, x=0.0, y=frame.cells_y)


class _Gallery(lbry._OnDemandCell):
    def __init__(self, lib):
        assert isinstance(lib, IOLibrary), "Internal error"

        super().__init__(lib, "Gallery")

    cells = (
        "IOPadVss", "IOPadVdd", "IOPadIn", "IOPadOut", "IOPadInOut",
        "IOPadIOVss", "IOPadIOVdd", "IOPadAnalog",
    )
    cells_l = tuple(cell.lower() for cell in cells)

    def _create_circuit(self):
        lib = self.lib
        ckt = self.new_circuit()
        insts = ckt.instances

        for cell in self.cells:
            ckt.new_instance(cell.lower(), lib.get_cell(cell))

        # vss and iovss are connected by the substrate
        # make only vss in Gallery so it is LVS clean.
        for net in ("vdd", "vss", "iovdd"):
            ports = tuple(insts[cell].ports[net] for cell in self.cells_l)
            if net == "vss":
                ports += tuple(insts[cell].ports["iovss"] for cell in self.cells_l)
            ckt.new_net(net, external=True, childports=ports)

        ckt.new_net("in_s", external=True, childports=(
            insts.iopadin.ports.s,
        ))
        ckt.new_net("in_pad", external=True, childports=(
            insts.iopadin.ports.pad,
        ))
        ckt.new_net("out_d", external=True, childports=(
            insts.iopadout.ports.d,
        ))
        ckt.new_net("out_de", external=True, childports=(
            insts.iopadout.ports.de,
        ))
        ckt.new_net("out_pad", external=True, childports=(
            insts.iopadout.ports.pad,
        ))
        ckt.new_net("io_s", external=True, childports=(
            insts.iopadinout.ports.s,
        ))
        ckt.new_net("io_d", external=True, childports=(
            insts.iopadinout.ports.d,
        ))
        ckt.new_net("io_de", external=True, childports=(
            insts.iopadinout.ports.de,
        ))
        ckt.new_net("io_pad", external=True, childports=(
            insts.iopadinout.ports.pad,
        ))
        ckt.new_net("ana_out", external=True, childports=(
            insts.iopadanalog.ports.pad,
        ))
        ckt.new_net("ana_outres", external=True, childports=(
            insts.iopadanalog.ports.padres,
        ))

    def _create_layout(self):
        ckt = self.circuit
        insts = ckt.instances
        layouter = self.new_circuitlayouter()

        x = 0.0
        y = 0.0

        l: Optional[lay._Layout] = None
        for cell in self.cells_l:
            l = layouter.place(insts[cell], x=x, y=y)
            x = cast(geo._Rectangular, l.boundary).right
        assert l is not None

        self.layout.boundary = geo.Rect(
            left=0.0, bottom=0.0, right=cast(geo._Rectangular, l.boundary).right,
            top=cast(geo._Rectangular, l.boundary).top,
        )


class IOLibrary(lbry.Library):
    def __init__(self,
        name=None, *, tech, cktfab=None, layoutfab=None, stdcelllib,
        nmos: prm.MOSFET, pmos: prm.MOSFET, ionmos: prm.MOSFET, iopmos: prm.MOSFET,
        cell_width: IntFloat, cell_height: IntFloat,
        ioring_maxwidth: IntFloat, ioring_space: IntFloat, ioring_spacetop: IntFloat,
        ioringvia_pitch: IntFloat, ioring_vddvss_y: IntFloat,
        pad_width: IntFloat, pad_height: IntFloat, pad_y: IntFloat,
        padvia_pitch: IntFloat, padvia_corner_distance: IntFloat, padvia_metal_enclosure: IntFloat,
        metal_bigspace: IntFloat, topmetal_bigspace: IntFloat,
        clampnmos: Optional[prm.MOSFET]=None, clampnmos_bottom: IntFloat,
        clampnmos_w: IntFloat, clampnmos_l: Optional[IntFloat]=None,
        clamppmos: Optional[prm.MOSFET]=None, clamppmos_bottom: IntFloat,
        clamppmos_w: IntFloat, clamppmos_l: Optional[IntFloat]=None,
        clampfingers: int, clampfingers_analog: Optional[int],
        clampgate_gatecont_space: IntFloat, clampgate_sourcecont_space: IntFloat,
        clampgate_draincont_space: IntFloat,
        clampdrain_layer: prm._MaskPrimitive, clampgate_clampdrain_overlap: IntFloat,
        clampdrain_active_ext: IntFloat, clampdrain_gatecont_space: IntFloat,
        clampdrain_via1columns: int,
        nres: prm.Resistor, pres: prm.Resistor, ndiode: prm.Diode, pdiode: prm.Diode,
        secondres_width: IntFloat, secondres_length: IntFloat,
        secondres_active_space: IntFloat, secondiovss_y: IntFloat,
        corerow_height: IntFloat, corerow_nwell_height: IntFloat,
        iorow_height: IntFloat, iorow_nwell_height: IntFloat,
        levelup_core_space: IntFloat,
    ):
        if name is None:
            name = tech.name + "_IO"
        super().__init__(name, tech=tech, cktfab=cktfab, layoutfab=layoutfab)

        if not isinstance(stdcelllib, lbry.Library):
            raise TypeError(
                f"stdcelllib has to be of type 'Library' not of '{type(stdcelllib)}'"
            )
        self.stdcelllib = stdcelllib

        self.iocell_width = _util.i2f(cell_width)
        self.iocell_height = _util.i2f(cell_height)

        self.ioring_maxwidth = _util.i2f(ioring_maxwidth)
        self.ioring_space = _util.i2f(ioring_space)
        self.ioring_spacetop = _util.i2f(ioring_spacetop)
        self.ioringvia_pitch = _util.i2f(ioringvia_pitch)

        self.pad_width = _util.i2f(pad_width)
        self.pad_height = _util.i2f(pad_height)
        self.padvia_pitch = _util.i2f(padvia_pitch)
        self.padvia_corner_distance = _util.i2f(padvia_corner_distance)
        self.padvia_metal_enclosure = _util.i2f(padvia_metal_enclosure)

        self.metal_bigspace = _util.i2f(metal_bigspace)
        self.topmetal_bigspace = _util.i2f(topmetal_bigspace)

        if clampnmos is None:
            clampnmos = ionmos
        if clamppmos is None:
            clamppmos = iopmos
        for name, mos in {
            "nmos": nmos, "pmos": pmos, "ionmos": ionmos, "iopmos": iopmos,
            "clampnmos": clampnmos, "clamppmos": clamppmos,
        }.items():
            if not isinstance(mos, prm.MOSFET):
                raise TypeError(f"{name} has to be of type 'MOSFET'")
            setattr(self, name, mos)
        # TODO: Implement proper source implant for transistor
        self.clampnmos_w = _util.i2f(clampnmos_w)
        if clampnmos_l is not None:
            self.clampnmos_l = _util.i2f(clampnmos_l)
        else:
            self.clampnmos_l = clampnmos.computed.min_l
        self.clamppmos_w = _util.i2f(clamppmos_w)
        if clamppmos_l is not None:
            self.clamppmos_l = _util.i2f(clamppmos_l)
        else:
            self.clamppmos_l = clamppmos.computed.min_l
        self.clampcount = clampfingers
        self.clampcount_analog = (
            clampfingers_analog if clampfingers_analog is not None
            else clampfingers
        )

        self.clampgate_gatecont_space = _util.i2f(clampgate_gatecont_space)
        self.clampgate_sourcecont_space = _util.i2f(clampgate_sourcecont_space)
        self.clampgate_draincont_space = _util.i2f(clampgate_draincont_space)

        self.clamp_clampdrain = clampdrain_layer
        self.clampgate_clampdrain_overlap = _util.i2f(clampgate_clampdrain_overlap)
        self.clampdrain_active_ext = _util.i2f(clampdrain_active_ext)
        self.clampdrain_gatecont_space = _util.i2f(clampdrain_gatecont_space)
        self.clampdrain_via1columns = clampdrain_via1columns

        self.nres = nres
        self.pres = pres
        self.ndiode = ndiode
        self.pdiode = pdiode
        self.secondres_width = _util.i2f(secondres_width)
        self.secondres_length = _util.i2f(secondres_length)
        self.secondres_active_space = _util.i2f(secondres_active_space)

        self.corerow_height = _util.i2f(corerow_height)
        self.corerow_nwell_height = _util.i2f(corerow_nwell_height)
        self.iorow_height = _util.i2f(iorow_height)
        self.iorow_nwell_height = _util.i2f(iorow_nwell_height)
        self.cells_height = self.corerow_height + self.iorow_height
        self.corerow_pwell_height = self.corerow_height - self.corerow_nwell_height
        self.iorow_pwell_height = self.iorow_height - self.iorow_nwell_height

        self.levelup_core_space = _util.i2f(levelup_core_space)

        self.computed = _ComputedSpecs(
            self, nmos=nmos, pmos=pmos, ionmos=ionmos, iopmos=iopmos,
        )

        # The cells have to be ordered so that a subcell instantiated in a cell
        # is coming first

        self.frame = _IOCellFrame(self,
            pad_y=pad_y, pclamp_y=clamppmos_bottom, nclamp_y=clampnmos_bottom,
            vddvss_y=ioring_vddvss_y, secondiovss_y=secondiovss_y,
        )

        # Add Top Gallery Cell
        self.cells += _Gallery(self)

    def guardring(self, *, type_, width, height, fill_well=False, fill_implant=False):
        s = "GuardRing_{}{}W{}H{}{}".format(
            type_.upper(),
            round(width/self.tech.grid),
            round(height/self.tech.grid),
            "T" if fill_well else "F",
            "T" if fill_implant else "F",
        )

        try:
            return self.cells[s]
        except KeyError:
            cell = _GuardRing(
                name=s, lib=self, type_=type_, width=width, height=height,
                fill_well=fill_well, fill_implant=fill_implant,
            )
            self.cells += cell
            return cell

    def iotracksegment(self, *, height):
        s = "IOTrackSegment_{}H".format(
            round(height/self.tech.grid),
        )

        try:
            return self.cells[s]
        except KeyError:
            cell = _IODCTrackSegment(name=s, lib=self, height=height)
            self.cells += cell
            return cell

    def ioduotracksegment(self, *, height, space):
        s = "IODuoTrackSegment_{}H{}S".format(
            round(height/self.tech.grid),
            round(space/self.tech.grid),
        )

        try:
            return self.cells[s]
        except KeyError:
            cell = _IODCDuoTrackSegment(
                name=s, lib=self, height=height, space=space,
            )
            self.cells += cell
            return cell

    def pad(self, *, width, height):
        s = "Pad_{}W{}H".format(
            round(width/self.tech.grid),
            round(height/self.tech.grid),
        )

        try:
            return self.cells[s]
        except KeyError:
            cell = _Pad(name=s, lib=self, width=width, height=height)
            self.cells += cell
            return cell

    def clamp(self, *, type_, n_trans, n_drive):
        s = "Clamp_{}{}N{}D".format(
            type_.upper(),
            n_trans,
            n_drive,
        )

        try:
            return self.cells[s]
        except KeyError:
            cell = _Clamp(
                name=s, type_=type_, lib=self, n_trans=n_trans, n_drive=n_drive,
            )
            self.cells += cell
            return cell

    def get_cell(self, name):
        try:
            return self.cells[name]
        except KeyError:
            lookup = {
                "SecondaryProtection": _Secondary,
                "LevelUp": _LevelUp,
                "LevelDown": _LevelDown,
                "GateDecode": _GateDecode,
                "CellsBulk": _CellsBulk,
                "IOPadIn": _PadIn,
                "IOPadOut": _PadOut,
                "IOPadInOut": _PadInOut,
                "IOPadVdd": _PadVdd,
                "IOPadVss": _PadVss,
                "IOPadIOVdd": _PadIOVdd,
                "IOPadIOVss": _PadIOVss,
                "IOPadAnalog": _PadAnalog,
            }
            cell = lookup[name](self)
            self.cells += cell
            return cell
